package org.alexsem.dice

import org.alexsem.dice.adventure.TestScenarioTemplate
import org.alexsem.dice.game.Game
import org.alexsem.dice.generator.generateHero
import org.alexsem.dice.generator.generateLocations
import org.alexsem.dice.generator.generateScenario
import org.alexsem.dice.menu.MainMenu
import org.alexsem.dice.model.Hero
import org.alexsem.dice.ui.ConsoleUI
import org.alexsem.dice.ui.audio.*
import org.alexsem.dice.ui.render.strings.PropertiesStringLoader
import java.lang.Exception
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*


fun main(args: Array<String>) {
    //Hide cursor on start
    print("\u001B[?25l")

    //Initialize audio system
    Audio.init(
            if ("nosound" in args) MuteSoundPlayer() else BasicSoundPlayer(),
            if ("nomusic" in args) MuteMusicPlayer() else BasicMusicPlayer()
    )

    //Prepare UI facade
    val savesDir = if (args.isNotEmpty())
        try {
            with(Paths.get(args[0])) {
                if (Files.exists(this) && Files.isDirectory(this)) args[0] else "."
            }
        } catch (ex: Exception) {
            "."
        } else "."
    val ui = ConsoleUI(PropertiesStringLoader(Locale.getDefault()), savesDir)

//    //TODO >>> delete below
//    val template = TestScenarioTemplate()
//    val scenario = generateScenario(template, 1)
//    val heroes = listOf(
//            generateHero(Hero.Type.BRAWLER, "Typical"),
//            generateHero(Hero.Type.HUNTER, "Hood"),
//            generateHero(Hero.Type.MYSTIC, "Wazza"),
//            generateHero(Hero.Type.PROPHET, "Mr. Miracle"),
//            generateHero(Hero.Type.MENTOR, "Jinxer")
//    )
//    val locations = generateLocations(template, 1, heroes.size)
//
//    val game = Game(ui, "someID", 0, scenario, locations, heroes) { _, _, _, _ -> }
//    game.start()

//    val interlude = generateInterlude(MysteriousWomanInterludeTemplate(), heroes)
//    val player = InterludePlayer(ui, interlude)
//    player.start()
    //TODO <<< delete above

    //Start main menu
    MainMenu(ui).start()

    //Re-display cursor on exit
    print("\u001B[?25h")
}