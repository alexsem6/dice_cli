package org.alexsem.dice.ui

import org.alexsem.dice.ui.input.*
import org.alexsem.dice.ui.render.*
import org.alexsem.dice.ui.render.strings.StringLoader
import org.alexsem.dice.ui.storage.BasicStorage

/**
 * Console implementation of renderers and interactors
 */
class ConsoleUI(loader: StringLoader, savesDir: String): UI {

    override val gameRenderer = ConsoleGameRenderer(loader)
    override val interludeRenderer = ConsoleInterludeRenderer(loader)
    override val managerRenderer = ConsoleManagerRenderer(loader)
    override val menuRenderer = ConsoleMenuRenderer(loader)
    override val upgradeRenderer = ConsoleUpgradeRenderer(loader)

    override val gameInteractor = ConsoleGameInteractor()
    override val interludeInteractor = ConsoleInterludeInteractor()
    override val managerInteractor = ConsoleManagerInteractor()
    override val menuInteractor = ConsoleMenuInteractor()
    override val upgradeInteractor = ConsoleUpgradeInteractor()

    override val storage = BasicStorage(savesDir)
}
