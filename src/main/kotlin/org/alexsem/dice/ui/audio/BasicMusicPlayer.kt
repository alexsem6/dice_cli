package org.alexsem.dice.ui.audio

import javazoom.jl.player.Player

/**
 * Basic implementation of [MusicPlayer]
 */
class BasicMusicPlayer : MusicPlayer {

    private var currentMusic: Music? = null
    private var thread: PlayerThread? = null

    /**
     * Return path to the specified music
     * @param music Music to get path for
     * @return Path to resource
     */
    private fun pathToFile(music: Music) = "/music/${music.toString().toLowerCase()}.mp3"

    override fun play(music: Music) {
        if (currentMusic == music) {
            return
        }
        currentMusic = music
        thread?.finish()
        Thread.yield()
        thread = PlayerThread(pathToFile(music))
        thread?.start()
    }

    override fun stop() {
        currentMusic = null
        thread?.finish()
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Thread responsible for playback
     */
    private inner class PlayerThread(private val musicPath: String) : Thread() {

        private lateinit var player: Player
        private var isLoaded = false
        private var isFinished = false

        init {
            isDaemon = true
        }

        override fun run() {
            loop@ while (!isFinished) {
                try {
                    player = Player(javaClass.getResource(musicPath).openConnection().apply {
                        useCaches = false
                    }.getInputStream())
                    isLoaded = true
                    player.play()
                } catch (ex: Exception) {
                    finish()
                    break@loop
                }
                player.close()
            }
        }

        fun finish() {
            isFinished = true
            this.interrupt()
            if (isLoaded) {
                player.close()
            }
        }
    }
}