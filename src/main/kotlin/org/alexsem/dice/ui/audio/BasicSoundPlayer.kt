package org.alexsem.dice.ui.audio

import javax.sound.sampled.AudioSystem

/**
 * Basic implementation of [SoundPlayer]
 */
class BasicSoundPlayer : SoundPlayer {

    /**
     * Return path to the specified sound
     * @param sound Sound to get path for
     * @return Path to resource
     */
    private fun pathToFile(sound: Sound) = "/sound/${sound.toString().toLowerCase()}.wav"

    override fun play(sound: Sound) {
        try {
            val url = javaClass.getResource(pathToFile(sound))
            AudioSystem.getClip().apply {
                open(AudioSystem.getAudioInputStream(url))
                start()
            }
        } catch (ex: Exception) {
            //Do nothing
        }
    }
}
