package org.alexsem.dice.ui.input

import org.alexsem.dice.game.HandMask
import org.alexsem.dice.game.PileMask
import org.alexsem.dice.ui.Action
import org.alexsem.dice.ui.Action.Type.*
import org.alexsem.dice.ui.ActionList

class ConsoleGameInteractor : ConsoleInteractor(), GameInteractor {

    override fun pickDiceFromHand(activePositions: HandMask, actions: ActionList): Action {
        while (true) {
            val key = read()
            actions.forEach { if (key mapsTo it.type && it.isEnabled) return it }
            when (key) {
                in '1'..'9' -> {
                    val index = key - '1'
                    if (activePositions.checkPosition(index)) {
                        return Action(HAND_POSITION, data = index)
                    }
                }
                '0' -> {
                    if (activePositions.checkPosition(9)) {
                        return Action(HAND_POSITION, data = 9)
                    }
                }
                in 'a'..'f' -> {
                    val allyIndex = key - 'a'
                    if (activePositions.checkAllyPosition(allyIndex)) {
                        return Action(HAND_ALLY_POSITION, data = allyIndex)
                    }
                }
            }
        }
    }

    override fun pickDiceFromPile(activePositions: PileMask, actions: ActionList): Action {
        while (true) {
            val key = read()
            actions.forEach { if (key mapsTo it.type && it.isEnabled) return it }
            val index = getIndexForKey(key)
            if (index > -1 && activePositions.checkPosition(index)) {
                return Action(PILE_POSITION, data = index)
            }
        }
    }

}