package org.alexsem.dice.ui.input

import jline.console.ConsoleReader
import jline.internal.NonBlockingInputStream
import org.alexsem.dice.ui.Action
import org.alexsem.dice.ui.Action.Type.*
import org.alexsem.dice.ui.ActionList

/**
 * Abstract superclass for all console interactors
 */
abstract class ConsoleInteractor : Interactor {

    private val reader = ConsoleReader()
    private val mapper = mapOf(
            CONFIRM to 13.toChar(),
            CANCEL to 27.toChar(),
            MENU to 27.toChar(),
            EXIT to 'x',

            EXPLORE_LOCATION to 'e',
            EXPLORE_LOCATION_AGAIN to 'r',
            CLOSE_LOCATION to 'c',
            USE_SKILL to 's',
            ASSIST to 'a',
            TRAVEL to 't',
            GIVE_DIE to 'g',
            FLEE to 'f',
            INFO to 'i',
            INFO_GENERAL to 'g',
            INFO_ENEMY to 'e',
            INFO_SPECIAL_RULES to 'r',
            INFO_PAGE_UP to 'b',
            INFO_PAGE_DOWN to 'n',
            INFO_VILLAIN to 'v',
            INFO_OBSTACLE to 'o',
            INFO_MODIFIERS to 'm',
            INFO_SCENARIO to 's',
            INFO_SCENARIO_ALLIES to 'a',
            INFO_LOCATION to 'l',
            INFO_HERO to 'h',
            INFO_HERO_DICE to 'd',
            INFO_HERO_SKILLS to 's',
            FINISH_TURN to 'f',
            ACQUIRE to 'a',
            ENDURE to 'e',
            LEAVE to 'l',
            FORFEIT to 'f',
            HIDE to 'h',
            DISCARD to 'd',
            DETER to 'd',

            PHYSICAL to 'p',
            SOMATIC to 's',
            MENTAL to 'm',
            VERBAL to 'v',
            DIVINE to 'd',
            ALLY to 'a',
            NEXT_HERO to '.',
            PREVIOUS_HERO to ',',
            FINISH to 'f',

            ADD_HERO to 'a',
            DELETE_HERO to 'd',
            RENAME_HERO to 'r',
            RENAME_PARTY to 'p',

            RETRY to 'r',
            DELETE_PARTY to 'd',
            NEXT_ADVENTURE to '.',
            PREVIOUS_ADVENTURE to ',',

            MANAGE_DICE to 'm',
            NEW_ADVENTURE to 'n',
            CONTINUE_ADVENTURE to 'c',
            MANUAL to 'm',
            PLAY_INTRO to 'i',
            PLAY_OUTRO to 'o'
    )

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Wait for character input from console
     * @return character that was pressed
     */
    protected fun read(): Char {
        val key = reader.readCharacter()
        if (key == 27) {
            val stream = reader.input as NonBlockingInputStream
            if (stream.peek(100) > -2) {
                while (stream.peek(100) > -2) {
                    stream.read()
                }
                return '\u0000'
            }
        }
        return key.toChar()
    }

    /**
     * Checks whether current character is the map for specified [Action]
     * @param type type of [Action] to check for
     * @return true or false
     */
    protected infix fun Char.mapsTo(type: Action.Type) = mapper[type] == this

    /**
     * Return item index corresponding to pressed key (with respect to current COUNTER)
     * @param key Key pressed
     * @return Respective index
     */
    protected open fun getIndexForKey(key: Char) = "1234567890abcdefghijklmnopqrstuvw".indexOf(key)

    //------------------------------------------------------------------------------------------------------------------

    override fun anyInput() {
        read()
    }

    override fun pickAction(list: ActionList): Action {
        while (true) {
            val key = read()
            list
                    .filter(Action::isEnabled)
                    .find { mapper[it.type] == key }
                    ?.let { return it }
        }
    }

    override fun pickItemFromList(listSize: Int): Action {
        while (true) {
            val key = read()
            when (key) {
                mapper[CANCEL] -> return Action(CANCEL)
                else -> {
                    val index = getIndexForKey(key)
                    if (index > -1 && index < listSize) {
                        return Action(Action.Type.LIST_ITEM, data = index)
                    }
                }
            }
        }
    }

}