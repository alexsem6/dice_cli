package org.alexsem.dice.ui.input

import org.alexsem.dice.ui.Action
import org.alexsem.dice.ui.Action.Type.*

class ConsoleInterludeInteractor : ConsoleInteractor(), InterludeInteractor {

    override fun advanceScript() =
            if (read().mapsTo(CANCEL)) Action(CANCEL) else Action(INFO_PAGE_DOWN)
}