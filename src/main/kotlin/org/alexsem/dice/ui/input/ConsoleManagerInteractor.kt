package org.alexsem.dice.ui.input

import org.alexsem.dice.ui.Action
import org.alexsem.dice.ui.Action.Type.*
import org.alexsem.dice.ui.ActionList

/**
 * Console (keyboard) implementation of [ManagerInteractor] interface
 */
class ConsoleManagerInteractor : ConsoleInteractor(), ManagerInteractor {

    override fun getIndexForKey(key: Char) = "1234567890-=".indexOf(key)

    override fun pickDiceManagerAction(actions: ActionList, commonPoolPositions: Set<Int>): Action {
        while (true) {
            val key = read()
            actions
                    .filter(Action::isEnabled)
                    .find { key mapsTo it.type }
                    ?.let { return it }
            val index = getIndexForKey(key)
            if (index in commonPoolPositions) {
                return Action(LIST_ITEM, data = index)
            }
        }
    }

    override fun inputText(initialText: String, maxLength: Int, allowSpaces: Boolean, allowDigits: Boolean, textChangeListener: ((String) -> Unit)?): String? {
        val input = StringBuilder(initialText)
        while (true) {
            val key = read()
            //Service keys
            when {
                key mapsTo CONFIRM -> if (input.isNotEmpty()) {
                    return input.toString()
                }
                key mapsTo CANCEL -> return null
                key == '\u0008' || key == '\u007f' -> if (input.isNotEmpty()) {
                    input.deleteCharAt(input.lastIndex)
                }
            }
            //Characters
            if (input.length < maxLength) {
                when {
                    key.isLetter() -> input.append(key)
                    key.isDigit() -> if (allowDigits && input.isNotEmpty()) {
                        input.append(key)
                    }
                    key == ' ' -> if (allowSpaces && input.isNotEmpty()) {
                        input.append(key)
                    }
                }
            }
            //Listener
            textChangeListener?.invoke(input.toString())
        }
    }
}
