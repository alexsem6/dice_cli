package org.alexsem.dice.ui.input

import org.alexsem.dice.ui.Action
import org.alexsem.dice.ui.ActionList

class ConsoleMenuInteractor : ConsoleInteractor(), MenuInteractor {

    private fun pickListOrAction(actions: ActionList, listSize: Int): Action {
        while (true) {
            val key = read()
            actions
                    .filter(Action::isEnabled)
                    .find { key mapsTo it.type }
                    ?.let { return it }
            val index = getIndexForKey(key)
            if (index > -1 && index < listSize) {
                return Action(Action.Type.LIST_ITEM, data = index)
            }
        }
    }

    override fun pickPartySelectorAction(actions: ActionList, currentPagePartyCount: Int) = pickListOrAction(actions, currentPagePartyCount)

    override fun pickScenarioSelectorAction(actions: ActionList, currentPageScenarioCount: Int) = pickListOrAction(actions, currentPageScenarioCount)

}