package org.alexsem.dice.ui.input

/**
 * Console (keyboard) implementation of [UpgradeInteractor] interface
 */
class ConsoleUpgradeInteractor : ConsoleInteractor(), UpgradeInteractor
