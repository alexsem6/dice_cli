package org.alexsem.dice.ui.render

import org.alexsem.dice.game.*
import org.alexsem.dice.ui.ActionList
import org.alexsem.dice.ui.StatusMessage
import org.alexsem.dice.ui.render.ConsoleRenderer.Color.*
import org.alexsem.dice.ui.render.strings.StringLoader
import org.alexsem.dice.model.*
import org.alexsem.dice.ui.Action
import org.fusesource.jansi.Ansi
import kotlin.math.max
import kotlin.math.min

class ConsoleGameRenderer(loader: StringLoader) : ConsoleRenderer(loader), GameRenderer {

    /**
     * Draw top info panel for location interior
     * @param location         Location to display
     * @param heroesAtLocation List of heroes at this location
     * @param currentHero      Current hero reference
     * @param timer            Current timer value
     */
    private fun drawLocationTopPanel(location: Location, heroesAtLocation: List<Hero>, currentHero: Hero, timer: Int) {
        val closedString = loadString("closed").toLowerCase()
        val timeString = loadString("time")
        val locationName = location.name[language].toUpperCase()
        val separatorX1 = locationName.length + if (location.isOpen) {
            6 + if (location.bag.size >= 10) 2 else 1
        } else {
            closedString.length + 7
        }
        val separatorX2 = CONSOLE_WIDTH - timeString.length - 6 - if (timer >= 10) 1 else 0
        //Top border
        ansi.cursor(1, 1)
        ansi.a('┌')
        (2 until CONSOLE_WIDTH).forEach { ansi.a(if (it == separatorX1 || it == separatorX2) '┬' else '─') }
        ansi.a('┐')
        //Center row
        ansi.cursor(2, 1)
        ansi.a("│ ")
        if (location.isOpen) {
            ansi.color(WHITE).a(locationName).reset()
            ansi.a(": ").a(location.bag.size)
        } else {
            ansi.a(locationName).reset()
            ansi.color(DARK_GRAY).a(" (").a(closedString).a(')').reset()
        }
        ansi.a(" │")
        var currentX = separatorX1 + 2
        heroesAtLocation.forEach { hero ->
            ansi.a(' ')
            ansi.color(heroColors[hero.type])
            ansi.a(if (hero === currentHero) '☻' else '☺').reset()
            currentX += 2
        }
        (currentX..separatorX2).forEach { ansi.a(' ') }
        ansi.a("│ ").a(timeString).a(": ")
        when {
            timer <= 5 -> ansi.color(LIGHT_RED)
            timer <= 15 -> ansi.color(LIGHT_YELLOW)
            else -> ansi.color(LIGHT_GREEN)
        }
        ansi.bold().a(timer).reset().a(" │")
        //Bottom border
        ansi.cursor(3, 1)
        ansi.a('├')
        (2 until CONSOLE_WIDTH).forEach { ansi.a(if (it == separatorX1 || it == separatorX2) '┴' else '─') }
        ansi.a('┤')
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Draw hero info panel for location interior
     * @param offsetY Vertical position
     * @param hero    Current hero
     */
    private fun drawLocationHeroPanel(offsetY: Int, hero: Hero) {
        val bagString = loadString("bag").toUpperCase()
        val discardString = loadString("discard").toUpperCase()
        val separatorX1 = hero.name.length + 4
        val separatorX3 = CONSOLE_WIDTH - discardString.length - 6 - if (hero.discardPile.size >= 10) 1 else 0
        val separatorX2 = separatorX3 - bagString.length - 6 - if (hero.bag.size >= 10) 1 else 0
        //Top border
        ansi.cursor(offsetY, 1)
        ansi.a('├')
        (2 until CONSOLE_WIDTH).forEach { ansi.a(if (it == separatorX1 || it == separatorX2 || it == separatorX3) '┬' else '─') }
        ansi.a('┤')
        //Center row
        ansi.cursor(offsetY + 1, 1)
        ansi.a("│ ")
        ansi.color(heroColors[hero.type]).a(hero.name.toUpperCase()).reset()
        ansi.a(" │")
        val currentX = separatorX1 + 1
        (currentX until separatorX2).forEach { ansi.a(' ') }
        ansi.a("│ ").a(bagString).a(": ")
        when {
            hero.bag.size <= hero.hand.capacity -> ansi.color(LIGHT_RED)
            else -> ansi.color(LIGHT_YELLOW)
        }
        ansi.a(hero.bag.size).reset()
        ansi.a(" │ ").a(discardString).a(": ")
        ansi.a(hero.discardPile.size)
        ansi.a(" │")
        //Bottom border
        ansi.cursor(offsetY + 2, 1)
        ansi.a('├')
        (2 until CONSOLE_WIDTH).forEach { ansi.a(if (it == separatorX1 || it == separatorX2 || it == separatorX3) '┴' else '─') }
        ansi.a('┤')
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Draws one specific die
     * @param die     Die to draw
     * @param checked true to draw die name as checked
     */
    private fun drawDieSize(die: Die, checked: Boolean = false) {
        when {
            checked -> ansi.background(dieColors[die.type]).color(BLACK)
            else -> ansi.color(dieColors[die.type])
        }
        ansi.a(die.toString()).reset()
    }

    /**
     * Draw frame big enough to fit the die size
     * @param offsetX     Horizontal position
     * @param offsetY     Vertical position
     * @param longDieSize true for d10 and d12, false for other dice
     */
    private fun drawDieFrameSmall(offsetX: Int, offsetY: Int, longDieSize: Boolean) {
        //Top border
        ansi.cursor(offsetY, offsetX)
        ansi.a('╔')
        (0 until if (longDieSize) 5 else 4).forEach { ansi.a('═') }
        ansi.a('╗')
        //Left border
        ansi.cursor(offsetY + 1, offsetX)
        ansi.a("║ ")
        //Bottom border
        ansi.cursor(offsetY + 2, offsetX)
        ansi.a("╚")
        (0 until if (longDieSize) 5 else 4).forEach { ansi.a('═') }
        ansi.a('╝')
        //Right border
        ansi.cursor(offsetY + 1, offsetX + if (longDieSize) 6 else 5)
        ansi.a('║')
    }

    /**
     * Draw die roll result (with modifier) using regular numbers
     * @param offsetX    Horizontal position
     * @param offsetY    Vertical position
     * @param pair       Die which size to draw
     * @param rollResult Number to display as roll result (or null)
     */
    private fun drawDieSmall(offsetX: Int, offsetY: Int, pair: DiePair, rollResult: Int? = null) {
        ansi.color(dieColors[pair.die.type])
        val longDieSize = pair.die.size >= 10
        drawDieFrameSmall(offsetX, offsetY, longDieSize)
        //Roll result or die size
        ansi.cursor(offsetY + 1, offsetX + 1)
        if (rollResult != null) {
            ansi.a(String.format(" %2d %s", rollResult, if (longDieSize) " " else ""))
        } else {
            ansi.a(' ').a(pair.die.toString()).a(' ')
        }
        //Draw modifier
        ansi.cursor(offsetY + 3, offsetX)
        val modString = if (pair.modifier == 0) "" else String.format("%+d", pair.modifier)
        val frameLength = 4 + if (longDieSize) 3 else 2
        var spaces = (frameLength - modString.length) / 2
        (0 until spaces).forEach { ansi.a(' ') }
        ansi.a(modString)
        spaces = frameLength - spaces - modString.length
        (0 until spaces).forEach { ansi.a(' ') }
        ansi.reset()
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Draw frame big enough to fit the die size
     * @param offsetX     Horizontal position
     * @param offsetY     Vertical position
     * @param longDieSize true for d10 and d12, false for other dice
     */
    private fun drawDieFrameBig(offsetX: Int, offsetY: Int, longDieSize: Boolean) {
        //Top border
        ansi.cursor(offsetY, offsetX)
        ansi.a('╔')
        (0 until if (longDieSize) 3 else 2).forEach { ansi.a("══════") }
        ansi.a("═╗")
        //Left border
        (1..5).forEach {
            ansi.cursor(offsetY + it, offsetX)
            ansi.a('║')
        }
        //Bottom border
        ansi.cursor(offsetY + 6, offsetX)
        ansi.a('╚')
        (0 until if (longDieSize) 3 else 2).forEach { ansi.a("══════") }
        ansi.a("═╝")
        //Right border
        val currentX = offsetX + if (longDieSize) 20 else 14
        (1..5).forEach {
            ansi.cursor(offsetY + it, currentX)
            ansi.a('║')
        }
    }

    /**
     * Draw die size (with modifier) using big numbers
     * @param offsetX Horizontal position
     * @param offsetY Vertical position
     * @param pair    Die which size to draw
     */
    private fun drawDieSizeBig(offsetX: Int, offsetY: Int, pair: DiePair) {
        ansi.color(dieColors[pair.die.type])
        val longDieSize = pair.die.size >= 10
        drawDieFrameBig(offsetX, offsetY, longDieSize)
        //Die size
        ansi.cursor(offsetY + 1, offsetX + 1)
        ansi.a(" ████  ")
        ansi.cursor(offsetY + 2, offsetX + 1)
        ansi.a(" █   █ ")
        ansi.cursor(offsetY + 3, offsetX + 1)
        ansi.a(" █   █ ")
        ansi.cursor(offsetY + 4, offsetX + 1)
        ansi.a(" █   █ ")
        ansi.cursor(offsetY + 5, offsetX + 1)
        ansi.a(" ████  ")
        drawBigNumber(offsetX + 8, offsetY + 1, pair.die.size)
        //Draw modifier
        ansi.cursor(offsetY + 7, offsetX)
        val modString = if (pair.modifier == 0) "" else String.format("%+d", pair.modifier)
        val frameLength = 4 + 6 * if (longDieSize) 3 else 2
        var spaces = (frameLength - modString.length) / 2
        (0 until spaces).forEach { ansi.a(' ') }
        ansi.a(modString)
        spaces = frameLength - spaces - modString.length - 1
        (0 until spaces).forEach { ansi.a(' ') }
        ansi.reset()
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Draws a number of dice which are going to participate in a check
     * @param offsetY     Vertical position
     * @param battleCheck DieBattleCheck object to render
     */
    private fun drawBattleCheck(offsetY: Int, battleCheck: DieBattleCheck) {
        val performCheck = loadString("perform_check")
        var currentX = 4
        var currentY = offsetY
        //Top message
        ansi.cursor(offsetY, 1)
        ansi.a("│  ").a(performCheck)
        (performCheck.length + 4 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')
        //Left border
        (1..4).forEach {
            ansi.cursor(offsetY + it, 1)
            ansi.a("│  ")
        }
        //Opponent
        var opponentWidth = 0
        var vsWidth = 0
        (battleCheck.getOpponentPair())?.let {
            //Die
            if (battleCheck.isRolled) {
                drawDieSmall(4, offsetY + 1, it, battleCheck.getOpponentResult())
            } else {
                drawDieSmall(4, offsetY + 1, it)
            }
            opponentWidth = 4 + if (it.die.size >= 10) 3 else 2
            currentX += opponentWidth
            //VS
            ansi.cursor(currentY + 1, currentX)
            ansi.a("    ")
            ansi.cursor(currentY + 2, currentX)
            ansi.color(LIGHT_YELLOW).a(" VS ").reset()
            ansi.cursor(currentY + 3, currentX)
            ansi.a("    ")
            ansi.cursor(currentY + 4, currentX)
            ansi.a("    ")
            vsWidth = 4
            currentX += vsWidth
        }
        //Clear below
        for (row in currentY + 5..currentY + 8) {
            ansi.cursor(row, 1)
            ansi.a('│')
            (2 until currentX).forEach { ansi.a(' ') }
        }
        //Dice
        for (index in 0 until battleCheck.heroPairCount) {
            if (index > 0) {
                ansi.cursor(currentY + 1, currentX)
                ansi.a("   ")
                ansi.cursor(currentY + 2, currentX)
                ansi.a(if (battleCheck.method == DieBattleCheck.Method.SUM) " + " else " / ").reset()
                ansi.cursor(currentY + 3, currentX)
                ansi.a("   ")
                ansi.cursor(currentY + 4, currentX)
                ansi.a("   ")
                currentX += 3
            }
            val pair = battleCheck.getHeroPairAt(index)
            val width = 4 + if (pair.die.size >= 10) 3 else 2
            if (currentX + width + 3 > CONSOLE_WIDTH) { //Out of space
                for (row in currentY + 1..currentY + 4) {
                    ansi.cursor(row, currentX)
                    (currentX until CONSOLE_WIDTH).forEach { ansi.a(' ') }
                    ansi.a('│')
                }
                currentY += 4
                currentX = 4 + vsWidth + opponentWidth
            }
            if (battleCheck.isRolled) {
                drawDieSmall(currentX, currentY + 1, pair, battleCheck.getHeroResultAt(index))
            } else {
                drawDieSmall(currentX, currentY + 1, pair)
            }
            currentX += width
        }
        //Clear the rest
        (currentY + 1..currentY + 4).forEach { row ->
            ansi.cursor(row, currentX)
            (currentX until CONSOLE_WIDTH).forEach { ansi.a(' ') }
            ansi.a('│')
        }
        if (currentY == offsetY) { //Still on the first line
            currentX = 4 + vsWidth + opponentWidth
            (currentY + 5..currentY + 8).forEach { row ->
                ansi.cursor(row, currentX)
                (currentX until CONSOLE_WIDTH).forEach { ansi.a(' ') }
                ansi.a('│')
            }
        }
        //Draw result
        (battleCheck.result)?.let { r ->
            val frameTopY = offsetY + 5
            val result = String.format("%+d", r)
            val message = loadString(if (r >= 0) "success" else "fail").toUpperCase()
            val color = if (r >= 0) DARK_GREEN else DARK_RED
            //Frame
            ansi.color(color)
            drawHorizontalLine(frameTopY, '▒')
            drawHorizontalLine(frameTopY + 3, '▒')
            ansi.cursor(frameTopY + 1, 1).a("▒▒")
            ansi.cursor(frameTopY + 1, CONSOLE_WIDTH - 1).a("▒▒")
            ansi.cursor(frameTopY + 2, 1).a("▒▒")
            ansi.cursor(frameTopY + 2, CONSOLE_WIDTH - 1).a("▒▒")
            ansi.reset()
            //Top message
            val resultString = loadString("result")
            var center = (CONSOLE_WIDTH - result.length - resultString.length - 2) / 2
            ansi.cursor(frameTopY + 1, 3)
            (3 until center).forEach { ansi.a(' ') }
            ansi.a(resultString).a(": ")
            ansi.color(color).a(result).reset()
            (center + result.length + resultString.length + 2 until CONSOLE_WIDTH - 1).forEach { ansi.a(' ') }
            //Bottom message
            center = (CONSOLE_WIDTH - message.length) / 2
            ansi.cursor(frameTopY + 2, 3)
            (3 until center).forEach { ansi.a(' ') }
            ansi.color(color).a(message).reset()
            (center + message.length until CONSOLE_WIDTH - 1).forEach { ansi.a(' ') }
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Draws the die which was encountered during exploration
     * @param offsetY   Vertical position
     * @param pair      Die (with modifier) which was encountered
     * @param encounter Encountered threat
     */
    private fun drawExplorationResult(offsetY: Int, pair: DiePair, encounter: Threat?) {
        val encountered = loadString("encountered")
        val encounterName = encounter?.let {
            when (pair.die.type) {
                Die.Type.OBSTACLE -> " " + it.name[language].toUpperCase()
                Die.Type.ENEMY -> " " + it.name[language].toUpperCase()
                Die.Type.VILLAIN -> " " + it.name[language].toUpperCase()
                else -> ""
            }
        } ?: ""
        ansi.cursor(offsetY, 1)
        ansi.a("│  ").a(encountered)
        ansi.color(dieColors[pair.die.type]).a(encounterName).reset().a(':')
        (encountered.length + 5 + encounterName.length until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')
        val dieFrameWidth = 3 + 6 * if (pair.die.size >= 10) 3 else 2
        for (row in 1..8) {
            ansi.cursor(offsetY + row, 1)
            ansi.a("│  ")
            ansi.cursor(offsetY + row, dieFrameWidth + 4)
            (dieFrameWidth + 4 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
            ansi.a('│')
        }
        drawDieSizeBig(4, offsetY + 1, pair)
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Draws one specific hand on console
     * @param offsetY         Line to begin input on (1-based)
     * @param hand            Hand to draw
     * @param checkedDice     List of dice to be displayed as checked
     * @param activePositions List of position numbers to be displayed as active
     */
    private fun drawHand(offsetY: Int, hand: Hand, checkedDice: HandMask, activePositions: HandMask) {
        val handString = loadString("hand").toUpperCase()
        val alliesString = loadString("allies").toUpperCase()
        val capacity = hand.capacity
        val size = hand.dieCount
        val slots = max(size, capacity)
        val alliesSize = hand.allyDieCount
        var currentY = offsetY
        var currentX = 1

        //Hand title
        ansi.cursor(currentY, currentX)
        ansi.a("│   ").a(handString)

        //Left border
        currentY += 1
        currentX = 1
        ansi.cursor(currentY, currentX)
        ansi.a("│ ╔")
        ansi.cursor(currentY + 1, currentX)
        ansi.a("│ ║")
        ansi.cursor(currentY + 2, currentX)
        ansi.a("│ ╚")
        ansi.cursor(currentY + 3, currentX)
        ansi.a("│  ")
        currentX += 3

        //Main hand
        for (i in 0 until min(slots, MAX_HAND_SIZE)) {
            val die = hand.dieAt(i)
            val longDieName = die != null && die.size >= 10

            //Top border
            ansi.cursor(currentY, currentX)
            if (i < capacity) {
                ansi.a("════").a(if (longDieName) "═" else "")
            } else {
                ansi.a("────").a(if (longDieName) "─" else "")
            }
            ansi.a(if (i < capacity - 1) '╤' else if (i == capacity - 1) '╗' else if (i < size - 1) '┬' else '┐')

            //Center row
            ansi.cursor(currentY + 1, currentX)
            ansi.a(' ')
            if (die != null) {
                drawDieSize(die, checkedDice.checkPosition(i))
            } else {
                ansi.a("  ")
            }
            ansi.a(' ')
            ansi.a(if (i < capacity - 1) '│' else if (i == capacity - 1) '║' else '│')

            //Bottom border
            ansi.cursor(currentY + 2, currentX)
            if (i < capacity) {
                ansi.a("════").a(if (longDieName) '═' else "")
            } else {
                ansi.a("────").a(if (longDieName) '─' else "")
            }
            ansi.a(if (i < capacity - 1) '╧' else if (i == capacity - 1) '╝' else if (i < size - 1) '┴' else '┘')

            //Die number
            ansi.cursor(currentY + 3, currentX)
            if (activePositions.checkPosition(i)) {
                ansi.color(LIGHT_YELLOW)
            }
            ansi.a(String.format(" (%s) %s", shortcut(i), if (longDieName) " " else ""))
            ansi.reset()

            currentX += 5 + if (longDieName) 1 else 0
        }

        //Ally subhand
        if (alliesSize > 0) {
            currentY = offsetY

            //Ally title
            ansi.cursor(currentY, handString.length + 5)
            (handString.length + 5 until currentX).forEach { ansi.a(' ') }
            ansi.a("     ").a(alliesString)
            (currentX + alliesString.length + 5 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
            ansi.a('│')

            //Left border
            currentY += 1
            ansi.cursor(currentY, currentX)
            ansi.a("   ┌")
            ansi.cursor(currentY + 1, currentX)
            ansi.a("   │")
            ansi.cursor(currentY + 2, currentX)
            ansi.a("   └")
            ansi.cursor(currentY + 3, currentX)
            ansi.a("    ")
            currentX += 4

            //Ally slots
            for (i in 0 until min(alliesSize, MAX_HAND_ALLY_SIZE)) {
                val allyDie = hand.allyDieAt(i)!!
                val longDieName = allyDie.size >= 10

                //Top border
                ansi.cursor(currentY, currentX)
                ansi.a("────").a(if (longDieName) "─" else "")
                ansi.a(if (i < alliesSize - 1) '┬' else '┐')

                //Center row
                ansi.cursor(currentY + 1, currentX)
                ansi.a(' ')
                drawDieSize(allyDie, checkedDice.checkAllyPosition(i))
                ansi.a(" │")

                //Bottom border
                ansi.cursor(currentY + 2, currentX)
                ansi.a("────").a(if (longDieName) "─" else "")
                ansi.a(if (i < alliesSize - 1) '┴' else '┘')

                //Die number
                ansi.cursor(currentY + 3, currentX)
                if (activePositions.checkAllyPosition(i)) {
                    ansi.color(LIGHT_YELLOW)
                }
                ansi.a(String.format(" (%s) %s", shortcut(i + 10), if (longDieName) " " else "")).reset()

                currentX += 5 + if (longDieName) 1 else 0
            }
        } else {
            ansi.cursor(offsetY, 9)
            (9 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
            ansi.a('│')
            ansi.cursor(offsetY + 4, currentX)
            (currentX until CONSOLE_WIDTH).forEach { ansi.a(' ') }
            ansi.a('│')
        }

        //Clear the end of the line
        (0..3).forEach { row ->
            ansi.cursor(currentY + row, currentX)
            (currentX until CONSOLE_WIDTH).forEach { ansi.a(' ') }
            ansi.a('│')
        }
    }

    //--------------------------------------------------------------------------

    /**
     * Draw the hero turn start screen
     * @param hero Hero to draw
     */
    override fun drawHeroTurnStart(hero: Hero) {
        val offsetY = (CONSOLE_HEIGHT - 5) / 2
        (1 until offsetY).forEach { drawBlankLine(it, false) }
        ansi.color(heroColors[hero.type])
        drawHorizontalLine(offsetY, '─')
        drawHorizontalLine(offsetY + 4, '─')
        ansi.reset()
        ansi.cursor(offsetY + 1, 1).eraseLine()
        ansi.cursor(offsetY + 3, 1).eraseLine()
        ansi.cursor(offsetY + 2, 1)
        val text = String.format(loadString("heros_turn"), hero.name.toUpperCase())
        val index = text.indexOf(hero.name.toUpperCase())
        val center = (CONSOLE_WIDTH - text.length) / 2
        ansi.cursor(offsetY + 2, center)
        ansi.eraseLine(Ansi.Erase.BACKWARD)
        ansi.a(text.substring(0, index))
        ansi.color(heroColors[hero.type]).a(hero.name.toUpperCase()).reset()
        ansi.a(text.substring(index + hero.name.length))
        ansi.eraseLine(Ansi.Erase.FORWARD)
        (offsetY + 5..CONSOLE_HEIGHT).forEach { drawBlankLine(it, false) }
        render()
    }

    //--------------------------------------------------------------------------

    override fun drawHeroDeath(hero: Hero) {
        val offsetY = (CONSOLE_HEIGHT - 5) / 2
        (1 until offsetY).forEach { drawBlankLine(it, false) }
        ansi.color(DARK_RED)
        drawHorizontalLine(offsetY, '┼')
        drawHorizontalLine(offsetY + 4, '┼')
        ansi.reset()
        drawBlankLine(offsetY + 1, false)
        drawBlankLine(offsetY + 3, false)
        val data = String.format(loadString("hero_died"), hero.name.toUpperCase())
        drawCenteredCaption(offsetY + 2, data, DARK_RED, false)
        (offsetY + 5..CONSOLE_HEIGHT).forEach { drawBlankLine(it, false) }
        render()
    }

    //------------------------------------------------------------------------------------------------------------------
    override fun drawLocationInteriorScreen(location: Location, heroesAtLocation: List<Hero>, timer: Int, currentHero: Hero,
                                            battleCheck: DieBattleCheck?, encounteredDie: DiePair?, encounteredThreat: Threat?,
                                            pickedDice: HandMask, activePositions: HandMask, statusMessage: StatusMessage,
                                            actions: ActionList) {
        //Top panel
        drawLocationTopPanel(location, heroesAtLocation, currentHero, timer)

        //Encounter info
        when {
            battleCheck != null -> drawBattleCheck(4, battleCheck)
            encounteredDie != null -> drawExplorationResult(4, encounteredDie, encounteredThreat)
            else -> (4..12).forEach { drawBlankLine(it) }
        }

        //Fill blank space
        val bottomHalfTop = CONSOLE_HEIGHT - 10 - calculateActionListHeight(actions, true)
        (13 until bottomHalfTop).forEach { drawBlankLine(it) }

        //Hero-specific info
        drawLocationHeroPanel(bottomHalfTop, currentHero)
        drawHand(bottomHalfTop + 3, currentHero.hand, pickedDice, activePositions)

        //Separator
        ansi.cursor(bottomHalfTop + 8, 1)
        ansi.a('├')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┤')

        //Status and actions
        drawStatusMessage(bottomHalfTop + 9, statusMessage)
        drawActionList(bottomHalfTop + 10, actions)

        //Bottom border
        ansi.cursor(CONSOLE_HEIGHT, 1)
        ansi.a('└')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┘')

        //Finalize
        render()
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Draw location image used for traveling animation
     * @param offsetX Horizontal position
     * @param offsetY Vertical position
     * @param isDestination true to draw images in brighter colors, false for dimmer colors
     */
    private fun drawLocationTravelImage(offsetX: Int, offsetY: Int, isDestination: Boolean) {
        val primaryColor = if (isDestination) Color.WHITE else Color.LIGHT_GRAY
        val secondaryColor = if (isDestination) Color.LIGHT_YELLOW else Color.DARK_GRAY
        val windowChar = if (isDestination) '▓' else '░'
        ansi.color(primaryColor)
        ansi.cursor(offsetY, offsetX)
        ansi.a("  _______  ")
        ansi.cursor(offsetY + 1, offsetX)
        ansi.a(" /       \\ ")
        ansi.cursor(offsetY + 2, offsetX)
        ansi.a("/_________\\")
        ansi.cursor(offsetY + 3, offsetX)
        ansi.a(" | ")
        ansi.color(secondaryColor)
        ansi.a("$windowChar   $windowChar")
        ansi.color(primaryColor)
        ansi.a(" | ")
        ansi.cursor(offsetY + 4, offsetX)
        ansi.a(" |_______| ")
        ansi.reset()
    }

    /**
     * Draw image of hero traveling from one location to another
     * @param offsetX Horizontal position
     * @param offsetY Vertical position
     * @param width Travel path width
     * @param height Travel path height
     * @param hero Traveling hero
     * @param progress Travel progress (from 0 to 100)
     */
    private fun drawHeroTravelProgress(offsetX: Int, offsetY: Int, width: Int, height: Int, hero: Hero, progress: Int) {
        //Blank lines
        (offsetY until offsetY + height - 3).forEach { row ->
            ansi.cursor(row, offsetX)
            (1..width).forEach { ansi.a(' ') }
        }
        //Hero
        val heroOffsetX = offsetX + (width - 3) * progress / 100
        val heroOffsetY = offsetY + height - 3
        ansi.color(heroColors[hero.type])
        ansi.cursor(heroOffsetY, heroOffsetX)
        ansi.a(" ☻ ")
        ansi.cursor(heroOffsetY + 1, heroOffsetX)
        ansi.a("/|\\")
        ansi.cursor(heroOffsetY + 2, heroOffsetX)
        ansi.a("/ \\")
        ansi.reset()
        //Blanks
        (offsetY + height - 3 until offsetY + height).forEach { row ->
            ansi.cursor(row, offsetX)
            (offsetX until heroOffsetX).forEach { ansi.a(' ') }
            ansi.cursor(row, heroOffsetX + 3)
            (heroOffsetX + 3 until offsetX + width).forEach { ansi.a(' ') }
        }
        ansi.reset()
    }

    override fun drawLocationTravelScreen(departureLocation: Location, destinationLocation: Location, hero: Hero, progress: Int, statusMessage: StatusMessage, actions: ActionList) {
        //Calculations
        val imageWidth = 11
        val imageHeight = 5
        val offsetY = (CONSOLE_HEIGHT - imageHeight - 2) / 2 - 1
        val departureName = departureLocation.name[language].toUpperCase()
        val destinationName = destinationLocation.name[language].toUpperCase()

        //Fill blank space
        (1 until offsetY).forEach { drawBlankLine(it, false) }


        //Location images
        val departureImageOffsetX = when {
            departureName.length > imageWidth -> (departureName.length - imageWidth) / 2 + 5
            else -> 5
        }
        val destinationImageOffsetX = when {
            destinationName.length > imageWidth -> CONSOLE_WIDTH - 3 - (imageWidth + destinationName.length) / 2
            else -> CONSOLE_WIDTH - imageWidth - 3
        }
        drawLocationTravelImage(departureImageOffsetX, offsetY, false)
        drawLocationTravelImage(destinationImageOffsetX, offsetY, true)
        (offsetY until offsetY + imageHeight).forEach { row ->
            ansi.cursor(row, 1)
            (1 until departureImageOffsetX).forEach { ansi.a(' ') }
            ansi.cursor(row, destinationImageOffsetX + imageWidth)
            (destinationImageOffsetX + imageWidth..CONSOLE_WIDTH).forEach { ansi.a(' ') }
        }

        //Hero progress
        drawHeroTravelProgress(departureImageOffsetX + imageWidth, offsetY, destinationImageOffsetX - departureImageOffsetX - imageWidth, imageHeight, hero, progress)

        //Blank line
        drawBlankLine(offsetY + imageHeight, false)

        //Location names
        val departureNameOffsetX = when {
            departureName.length > imageWidth -> 5
            else -> (imageWidth - departureName.length) / 2 + 5
        }
        val destinationNameOffsetX = when {
            destinationName.length > imageWidth -> CONSOLE_WIDTH - destinationName.length - 3
            else -> CONSOLE_WIDTH - (destinationName.length + imageWidth) / 2 - 3
        }
        ansi.cursor(offsetY + imageHeight + 1, 1)
        (1 until departureNameOffsetX).forEach { ansi.a(' ') }
        ansi.a(departureName)
        var offsetX = departureNameOffsetX + departureName.length
        (offsetX until destinationNameOffsetX).forEach { ansi.a(' ') }
        ansi.color(LIGHT_YELLOW).a(destinationName).reset()
        offsetX = destinationNameOffsetX + destinationName.length
        (offsetX..CONSOLE_WIDTH).forEach { ansi.a(' ') }

        //Blank line
        (offsetY + imageHeight + 2 until CONSOLE_HEIGHT - 3).forEach { drawBlankLine(it, false) }

        //Separator
        ansi.cursor(CONSOLE_HEIGHT - 3, 1)
        ansi.a('┌')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┐')

        //Status and actions
        drawStatusMessage(CONSOLE_HEIGHT - 2, statusMessage)
        drawActionList(CONSOLE_HEIGHT - 1, actions)

        //Bottom border
        ansi.cursor(CONSOLE_HEIGHT, 1)
        ansi.a('└')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┘')

        //Finalize
        render()
    }

    //------------------------------------------------------------------------------------------------------------------
    override fun drawHeroSelectionDialog(heroes: List<Hero>, statusMessage: StatusMessage, actions: ActionList) {
        //Calculations
        val contentHeight = heroes.size
        val contentWidth = 6 + (heroes.map { it.name.length }.max() ?: 0)
        val offsetY = (CONSOLE_HEIGHT - contentHeight) / 2 - 1
        val offsetX = (CONSOLE_WIDTH - contentWidth) / 2 - 1

        //Fill blank space
        (1 until offsetY - 2).forEach { drawBlankLine(it, false) }

        //Title
        ansi.cursor(offsetY - 2, offsetX + 2)
        ansi.eraseLine(Ansi.Erase.BACKWARD)
        ansi.a(loadString("choose_hero"))
        ansi.eraseLine(Ansi.Erase.FORWARD)

        //Top frame
        ansi.cursor(offsetY - 1, offsetX)
        ansi.eraseLine(Ansi.Erase.BACKWARD)
        ansi.a('╔')
        (0 until contentWidth).forEach { ansi.a('═') }
        ansi.a('╗')
        ansi.eraseLine(Ansi.Erase.FORWARD)

        //Dialog contents
        heroes.forEachIndexed { index, hero ->
            ansi.cursor(offsetY + index, offsetX)
            ansi.eraseLine(Ansi.Erase.BACKWARD)
            ansi.a('║')
            ansi.color(LIGHT_YELLOW).a(" (${shortcut(index)}) ").reset()
            ansi.color(heroColors[hero.type]).a(hero.name.toUpperCase()).reset()
            (offsetX + 7 + hero.name.length until offsetX + contentWidth + 2).forEach { ansi.a(' ') }
            ansi.a('║')
            ansi.eraseLine(Ansi.Erase.FORWARD)
        }

        //Bottom frame
        ansi.cursor(offsetY + contentHeight, offsetX)
        ansi.eraseLine(Ansi.Erase.BACKWARD)
        ansi.a('╚')
        (0 until contentWidth).forEach { ansi.a('═') }
        ansi.a('╝')
        ansi.eraseLine(Ansi.Erase.FORWARD)

        //Fill blank space
        (offsetY + contentHeight + 1 until CONSOLE_HEIGHT - 3).forEach { drawBlankLine(it, false) }

        //Separator
        ansi.cursor(CONSOLE_HEIGHT - 3, 1)
        ansi.a('┌')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┐')

        //Status and actions
        drawStatusMessage(CONSOLE_HEIGHT - 2, statusMessage)
        drawActionList(CONSOLE_HEIGHT - 1, actions)

        //Bottom border
        ansi.cursor(CONSOLE_HEIGHT, 1)
        ansi.a('└')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┘')

        //Finalize
        render()
    }

    //------------------------------------------------------------------------------------------------------------------
    override fun drawSkillSelectionDialog(skills: List<Skill>, allySkills: List<AllySkill>, statusMessage: StatusMessage, actions: ActionList) {
        //Calculations
        val skillNames = skills.map { stringHelper.loadSkillName(it.type).toUpperCase() }
        val allySkillNames = allySkills.map { stringHelper.loadAllySkillName(it.type).toUpperCase() }
        val lvl = loadString("lvl")
        val contentHeight = skills.size + allySkills.size
        val contentWidth = 6 + ((skillNames.map { it.length + lvl.length + 5 } + allySkillNames.map { it.length }).max()
                ?: 0)
        val offsetY = (CONSOLE_HEIGHT - contentHeight) / 2 - 1
        val offsetX = (CONSOLE_WIDTH - contentWidth) / 2 - 1

        //Fill blank space
        (1 until offsetY - 2).forEach { drawBlankLine(it, false) }

        //Title
        ansi.cursor(offsetY - 2, offsetX + 2)
        ansi.eraseLine(Ansi.Erase.BACKWARD)
        ansi.a(loadString("choose_skill"))
        ansi.eraseLine(Ansi.Erase.FORWARD)

        //Top frame
        ansi.cursor(offsetY - 1, offsetX)
        ansi.eraseLine(Ansi.Erase.BACKWARD)
        ansi.a('╔')
        (0 until contentWidth).forEach { ansi.a('═') }
        ansi.a('╗')
        ansi.eraseLine(Ansi.Erase.FORWARD)

        //Dialog contents
        var h = 0
        skills.forEach { skill ->
            ansi.cursor(offsetY + h, offsetX)
            ansi.eraseLine(Ansi.Erase.BACKWARD)
            ansi.a('║')
            val name = skillNames[h]
            ansi.color(LIGHT_YELLOW)
            ansi.a(" (${shortcut(h)}) ").reset()
            ansi.a(name)
            ansi.a(" ($lvl.${skill.level})")
            (offsetX + 7 + name.length + lvl.length + 5 until offsetX + contentWidth + 2).forEach { ansi.a(' ') }
            ansi.a('║')
            ansi.eraseLine(Ansi.Erase.FORWARD)
            h++
        }
        (0 until allySkills.size).forEach { index ->
            ansi.cursor(offsetY + h, offsetX)
            ansi.eraseLine(Ansi.Erase.BACKWARD)
            ansi.a('║')
            val name = allySkillNames[index]
            ansi.color(WHITE)
            ansi.a(" (${shortcut(h)}) ").reset()
            ansi.a(name)
            (offsetX + 7 + name.length until offsetX + contentWidth + 2).forEach { ansi.a(' ') }
            ansi.a('║')
            ansi.eraseLine(Ansi.Erase.FORWARD)
            h++
        }

        //Bottom frame
        ansi.cursor(offsetY + contentHeight, offsetX)
        ansi.eraseLine(Ansi.Erase.BACKWARD)
        ansi.a('╚')
        (0 until contentWidth).forEach { ansi.a('═') }
        ansi.a('╝')
        ansi.eraseLine(Ansi.Erase.FORWARD)

        //Fill blank space
        (offsetY + contentHeight + 1 until CONSOLE_HEIGHT - 3).forEach { drawBlankLine(it, false) }

        //Separator
        ansi.cursor(CONSOLE_HEIGHT - 3, 1)
        ansi.a('┌')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┐')

        //Status and actions
        drawStatusMessage(CONSOLE_HEIGHT - 2, statusMessage)
        drawActionList(CONSOLE_HEIGHT - 1, actions)

        //Bottom border
        ansi.cursor(CONSOLE_HEIGHT, 1)
        ansi.a('└')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┘')

        //Finalize
        render()
    }

    //------------------------------------------------------------------------------------------------------------------
    override fun drawPileDiceSelectionDialog(pile: Pile, pickedDice: PileMask, activePositions: PileMask, statusMessage: StatusMessage, actions: ActionList) {
        val dicePerRow = 11
        val dice = pile.examine()
        val size = min(dice.size, 33)
        var currentY = 1
        var currentX: Int

        //Blank lines
        drawBlankLine(currentY++, false)
        drawBlankLine(currentY++, false)

        //Title
        ansi.cursor(currentY, 7)
        ansi.eraseLine(Ansi.Erase.BACKWARD)
        ansi.a(loadString("pile").toUpperCase()).a(':')
        ansi.eraseLine(Ansi.Erase.FORWARD)
        currentY++

        //Blank lines
        drawBlankLine(currentY++, false)

        //Dice
        val batchCount = size / dicePerRow + if (size % dicePerRow == 0) 0 else 1
        for (batch in 0 until batchCount) {
            //Left border
            currentX = 7
            ansi.cursor(currentY, currentX).eraseLine(Ansi.Erase.BACKWARD)
            ansi.a('┌')
            ansi.cursor(currentY + 1, currentX).eraseLine(Ansi.Erase.BACKWARD)
            ansi.a('│')
            ansi.cursor(currentY + 2, currentX).eraseLine(Ansi.Erase.BACKWARD)
            ansi.a('└')
            ansi.cursor(currentY + 3, currentX).eraseLine(Ansi.Erase.BACKWARD)
            ansi.a(' ')
            currentX++

            //Die frames
            val startIndex = batch * dicePerRow
            val endIndex = min((batch + 1) * dicePerRow, size)
            for (index in startIndex until endIndex) {
                val die = dice[index]
                val longDieName = die.size >= 10

                //Top border
                ansi.cursor(currentY, currentX)
                ansi.a("────").a(if (longDieName) "─" else "")
                ansi.a(if (index < endIndex - 1) '┬' else '┐')

                //Center row
                ansi.cursor(currentY + 1, currentX)
                ansi.a(' ')
                drawDieSize(die, pickedDice.checkPosition(index))
                ansi.a(' ')
                ansi.a('│')

                //Bottom border
                ansi.cursor(currentY + 2, currentX)
                ansi.a("────").a(if (longDieName) '─' else "")
                ansi.a(if (index < endIndex - 1) '┴' else '┘')

                //Die number
                ansi.cursor(currentY + 3, currentX)
                if (activePositions.checkPosition(index)) {
                    ansi.color(LIGHT_YELLOW)
                }
                ansi.a(" (${shortcut(index)}) ")
                if (longDieName) {
                    ansi.a(' ')
                }
                ansi.reset()

                currentX += 5 + if (longDieName) 1 else 0
            }

            //Clear the end of the line
            (0..3).forEach {
                ansi.cursor(currentY + it, currentX)
                ansi.eraseLine(Ansi.Erase.FORWARD)
            }
            currentY += 4
        }

        //Fill blank space
        (currentY until CONSOLE_HEIGHT - 3).forEach { drawBlankLine(it, false) }

        //Separator
        ansi.cursor(CONSOLE_HEIGHT - 3, 1)
        ansi.a('┌')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┐')

        //Status and actions
        drawStatusMessage(CONSOLE_HEIGHT - 2, statusMessage)
        drawActionList(CONSOLE_HEIGHT - 1, actions)

        //Bottom border
        ansi.cursor(CONSOLE_HEIGHT, 1)
        ansi.a('└')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┘')

        //Finalize
        render()
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Draw top info panel for location exterior
     * @param scenario Scenario to display
     * @param timer    Current timer value
     */
    private fun drawScenarioTopPanel(scenario: Scenario, timer: Int) {
        val timeString = loadString("time")
        val scenarioName = scenario.name[language].toUpperCase()
        val separatorX2 = CONSOLE_WIDTH - timeString.length - 6 - if (timer >= 10) 1 else 0
        //Top border
        ansi.cursor(1, 1)
        ansi.a('┌')
        (2 until CONSOLE_WIDTH).forEach { ansi.a(if (it == separatorX2) '┬' else '─') }
        ansi.a('┐')
        //Center row
        ansi.cursor(2, 1)
        ansi.a("│ ")
        ansi.color(LIGHT_YELLOW).a(scenarioName).reset()
        val currentX = scenarioName.length + 4
        (currentX..separatorX2).forEach { ansi.a(' ') }
        ansi.a("│ ").a(timeString).a(": ")
        when {
            timer <= 5 -> ansi.color(LIGHT_RED)
            timer <= 15 -> ansi.color(LIGHT_YELLOW)
            else -> ansi.color(LIGHT_GREEN)
        }
        ansi.a(timer).reset().a(" │")
        //Bottom border
        ansi.cursor(3, 1)
        ansi.a('└')
        (2 until CONSOLE_WIDTH).forEach { ansi.a(if (it == separatorX2) '┴' else '─') }
        ansi.a('┘')
    }

    override fun drawLocationSelectionDialog(scenario: Scenario, timer: Int, locations: List<Location>, heroPlacement: Map<Location, List<Hero>>, currentHero: Hero, statusMessage: StatusMessage, actions: ActionList) {
        val closed = loadString("closed").toLowerCase()
        val locationsWithNames = locations.map { it to it.name[language].toUpperCase() }
        val paddingX = 4
        val separatorX1 = paddingX + 7
        val separatorX2 = separatorX1 + 3 + (locationsWithNames.map {
            when {
                it.first.isOpen -> it.second.length + if (it.first.bag.size >= 10) 4 else 3
                else -> it.second.length + closed.length + 3
            }
        }.max() ?: 0)
        var currentY = 7

        //Top scenario panel
        drawScenarioTopPanel(scenario, timer)
        drawHorizontalLine(4, ' ')
        drawHorizontalLine(5, ' ')
        drawHorizontalLine(6, ' ')

        //Title
        ansi.cursor(currentY, paddingX + 2)
        ansi.eraseLine(Ansi.Erase.BACKWARD)
        ansi.a(loadString("choose_location"))
        ansi.eraseLine(Ansi.Erase.FORWARD)
        currentY++

        //Top frame
        ansi.cursor(currentY, paddingX + 1)
        ansi.eraseLine(Ansi.Erase.BACKWARD)
        ansi.a('╔')
        (paddingX + 2 until CONSOLE_WIDTH - paddingX).forEach { ansi.a(if (it == separatorX1 || it == separatorX2) '╤' else '═') }
        ansi.a('╗')
        ansi.eraseLine(Ansi.Erase.FORWARD)
        currentY++

        //Location list
        locationsWithNames.forEachIndexed { index, pair ->
            ansi.cursor(currentY, paddingX + 1)
            ansi.eraseLine(Ansi.Erase.BACKWARD)
            ansi.a('║')
            ansi.color(LIGHT_YELLOW).a(" (${index + 1}) ").reset()
            ansi.a("│ ")
            var currentX = separatorX1 + pair.second.length + 2
            //Draw location name
            if (pair.first.isOpen) {
                ansi.color(WHITE).a(pair.second).reset()
                with(pair.first.bag.size) {
                    ansi.a(": ").a(this)
                    currentX += if (this >= 10) 4 else 3
                }
            } else {
                ansi.a(pair.second).reset()
                ansi.color(DARK_GRAY)
                currentX += closed.length + 3
                ansi.a(" (").a(closed).a(')').reset()
            }
            (currentX until separatorX2).forEach { ansi.a(' ') }
            ansi.a('│')
            currentX = separatorX2 + 1
            //Hero placement
            heroPlacement[pair.first]?.forEach { hero ->
                ansi.a(' ')
                ansi.color(heroColors[hero.type])
                ansi.a(if (hero === currentHero) '☻' else '☺').reset()
                currentX += 2
            }
            (currentX until CONSOLE_WIDTH - paddingX).forEach { ansi.a(' ') }
            ansi.a('║')
            ansi.eraseLine(Ansi.Erase.FORWARD)
            currentY++
        }

        //Bottom frame
        ansi.cursor(currentY, paddingX + 1)
        ansi.eraseLine(Ansi.Erase.BACKWARD)
        ansi.a('╚')
        (paddingX + 2 until CONSOLE_WIDTH - paddingX).forEach { ansi.a(if (it == separatorX1 || it == separatorX2) '╧' else '═') }
        ansi.a('╝')
        ansi.eraseLine(Ansi.Erase.FORWARD)

        //Fill blank space
        (currentY + 1 until CONSOLE_HEIGHT - 3).forEach { drawBlankLine(it, false) }

        //Separator
        ansi.cursor(CONSOLE_HEIGHT - 3, 1)
        ansi.a('┌')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┐')

        //Status and actions
        drawStatusMessage(CONSOLE_HEIGHT - 2, statusMessage)
        drawActionList(CONSOLE_HEIGHT - 1, actions)

        //Bottom border
        ansi.cursor(CONSOLE_HEIGHT, 1)
        ansi.a('└')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┘')

        //Finalize
        render()
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Generic function for calculating number of pages needed to display list of some items
     * @param items List of items to display
     * @param descriptionLoader Function used to load item description
     */
    private fun <T> calculateGenericInfoListPageCount(items: List<T>, descriptionLoader: (T) -> String): Int {
        val pageHeight = CONSOLE_HEIGHT - 5 /*top*/ - 4 /*bottom*/
        var pageCount = 1
        var currentPageOffset = 0
        items.forEach { item ->
            val requiredHeight = 1 + calculateMultilineTextHeight(descriptionLoader(item))
            if (currentPageOffset + requiredHeight > pageHeight) {
                pageCount++
                currentPageOffset = 0
            }
            currentPageOffset += requiredHeight + 1
        }
        return pageCount
    }

    /**
     * Generic function for drawing list of some items (names and description)
     * @param topPanelDrawer Function to draw top panel
     * @param title List title
     * @param items List of items to draw
     * @param nameLoader Function to load item name
     * @param nameAddition Function to draw additional info next to the name
     * @param descriptionLoader Function to load item descriptions
     * @param page Current page
     * @param totalPages Number of pages
     * @param statusMessage Status message
     * @param actions List of actions
     */
    private fun <T> drawGenericInfoList(topPanelDrawer: () -> Unit, title: String, items: List<T>, nameLoader: (T) -> String, nameAddition: ((T) -> Int)? = null, descriptionLoader: (T) -> String, page: Int, totalPages: Int, statusMessage: StatusMessage, actions: ActionList) {
        topPanelDrawer()
        var currentY = 4

        //Page title
        ansi.cursor(currentY, 1)
        val pagesText = if (totalPages > 1) " ($page/$totalPages)" else ""
        ansi.a("│ ")
        ansi.a(title.toUpperCase()).a(pagesText).a(':')
        (3 + title.length + pagesText.length + 1 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')
        currentY++

        //Blank line
        drawBlankLine(currentY, true)
        currentY++

        //Items
        val pageHeight = CONSOLE_HEIGHT - 5 /*top*/ - 4 /*bottom*/
        var currentPage = 1
        var currentPageOffset = 0
        for (item in items) {
            //Calculation
            val description = descriptionLoader(item)
            val requiredHeight = 1 + calculateMultilineTextHeight(description)
            if (currentPageOffset + requiredHeight > pageHeight) {
                currentPage++
                currentPageOffset = 0
            }
            currentPageOffset += requiredHeight + 1
            if (currentPage > page) {
                break
            } else if (currentPage < page) {
                continue
            }
            //Name
            ansi.cursor(currentY, 1)
            ansi.a("│ ")
            val name = nameLoader(item).toUpperCase()
            ansi.color(LIGHT_YELLOW).a(name).reset()
            val additionLength = nameAddition?.invoke(item) ?: 0
            (3 + name.length + additionLength until CONSOLE_WIDTH).forEach { ansi.a(' ') }
            ansi.a('│')
            currentY++
            //Description
            currentY += drawMultilineText(currentY, description)
            //Blank line
            drawBlankLine(currentY)
            currentY++
        }

        //Blank lines
        (currentY until CONSOLE_HEIGHT - 3).forEach { drawBlankLine(it) }

        //Separator
        ansi.cursor(CONSOLE_HEIGHT - 3, 1)
        ansi.a('├')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┤')

        //Status and actions
        drawStatusMessage(CONSOLE_HEIGHT - 2, statusMessage)
        drawActionList(CONSOLE_HEIGHT - 1, actions)

        //Bottom border
        ansi.cursor(CONSOLE_HEIGHT, 1)
        ansi.a('└')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┘')

        //Finalize
        render()
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Draws top panel with scenario name
     * @param scenario Scenario to draw
     */
    private fun drawScenarioTopPanel(scenario: Scenario) {
        //Top border
        ansi.cursor(1, 1)
        ansi.a('┌')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┐')
        //Center row
        val name = scenario.name[language].toUpperCase()
        drawCenteredCaption(2, name, LIGHT_YELLOW, true)
        //Bottom border
        ansi.cursor(3, 1)
        ansi.a('├')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┤')
    }

    override fun drawScenarioInfoGeneral(scenario: Scenario, deterrentPile: Pile, page: Int, totalPages: Int, statusMessage: StatusMessage, actions: ActionList) {
        drawScenarioTopPanel(scenario)
        var currentY = 4

        //Page title
        ansi.cursor(currentY, 1)
        val generalInfo = loadString("general_info")
        val pagesText = if (totalPages > 1) " ($page/$totalPages)" else ""
        ansi.a("│ ")
        ansi.a(generalInfo.toUpperCase()).a(pagesText).a(':')
        (3 + generalInfo.length + pagesText.length + 1 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')
        currentY++

        //Blank line
        drawBlankLine(currentY, true)
        currentY++

        if (page == 1) {
            //Level
            ansi.cursor(currentY, 1)
            ansi.a("│ ")
            val levelTitle = loadString("level")
            val levelValue = scenario.level.toString()
            ansi.a(levelTitle).a(": ")
            ansi.color(LIGHT_YELLOW).a(levelValue).reset()
            (3 + levelTitle.length + 2 + levelValue.length until CONSOLE_WIDTH).forEach { ansi.a(' ') }
            ansi.a('│')
            currentY++

            //Initial timer
            ansi.cursor(currentY, 1)
            ansi.a("│ ")
            val timerTitle = loadString("initial_timer")
            val timerValue = scenario.initialTimer.toString()
            ansi.a(timerTitle).a(": ")
            ansi.color(LIGHT_YELLOW).a(timerValue).reset()
            (3 + timerTitle.length + 2 + timerValue.length until CONSOLE_WIDTH).forEach { ansi.a(' ') }
            ansi.a('│')
            currentY++

            //Blank line
            drawBlankLine(currentY, true)
            currentY++
        }

        //Description
        val pageHeight = CONSOLE_HEIGHT - 5 /*top*/ - 4/*bottom*/
        val startLine = when (page) {
            1 -> 1
            else -> (pageHeight - 3) + (page - 2) * pageHeight + 1
        }
        val endLine = when (page) {
            1 -> pageHeight - 3
            else -> (pageHeight - 3) + (page - 1) * pageHeight
        }
        val description = scenario.description[language]
        currentY += drawMultilineText(currentY, description, true, startLine, endLine)

        //Deterrent pile
        if (page == totalPages) {
            drawBlankLine(currentY++)
            currentY += drawDiceList(currentY, loadString("deterrent_pile"), deterrentPile.examine())
        }
        //Blank lines
        (currentY until CONSOLE_HEIGHT - 3).forEach { drawBlankLine(it) }

        //Separator
        ansi.cursor(CONSOLE_HEIGHT - 3, 1)
        ansi.a('├')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┤')

        //Status and actions
        drawStatusMessage(CONSOLE_HEIGHT - 2, statusMessage)
        drawActionList(CONSOLE_HEIGHT - 1, actions)

        //Bottom border
        ansi.cursor(CONSOLE_HEIGHT, 1)
        ansi.a('└')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┘')

        //Finalize
        render()
    }

    override fun calculateScenarioInfoGeneralPageCount(scenario: Scenario): Int {
        val pageHeight = CONSOLE_HEIGHT - 5 /*top*/ - 4 /*bottom*/
        return with(calculateMultilineTextHeight(scenario.description[language]) + 3 - (pageHeight - 3)) {
            when {
                (this > 0) -> 1 + this / pageHeight + if (this % pageHeight > 0) 1 else 0
                else -> 1
            }
        }
    }

    override fun calculateScenarioInfoRulesPageCount(scenario: Scenario) =
            calculateGenericInfoListPageCount(scenario.getSpecialRules(), stringHelper::loadSpecialRuleDescription)

    override fun drawScenarioInfoRules(scenario: Scenario, page: Int, totalPages: Int, statusMessage: StatusMessage, actions: ActionList) =
            drawGenericInfoList({ drawScenarioTopPanel(scenario) }, loadString("special_rules"), scenario.getSpecialRules(),
                    stringHelper::loadSpecialRuleName, null, stringHelper::loadSpecialRuleDescription, page, totalPages, statusMessage, actions)

    override fun calculateScenarioInfoAlliesPageCount(scenario: Scenario) =
            calculateGenericInfoListPageCount(scenario.getAllySkills(), stringHelper::loadAllySkillDescription)

    override fun drawScenarioInfoAllies(scenario: Scenario, page: Int, totalPages: Int, statusMessage: StatusMessage, actions: ActionList) =
            drawGenericInfoList({ drawScenarioTopPanel(scenario) }, loadString("allies_skills"), scenario.getAllySkills(),
                    stringHelper::loadAllySkillName, null, stringHelper::loadAllySkillDescription, page, totalPages, statusMessage, actions)

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Draws top panel with location name
     * @param location Location to draw
     */
    private fun drawLocationTopPanel(location: Location) {
        //Top border
        ansi.cursor(1, 1)
        ansi.a('┌')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┐')
        //Center row
        val name = location.name[language].toUpperCase()
        if (location.isOpen) {
            drawCenteredCaption(2, name, LIGHT_YELLOW, true)
        } else {
            val closed = loadString("closed").toUpperCase()
            val length = name.length + 2 + closed.length + 1
            val center = (CONSOLE_WIDTH - length) / 2
            ansi.cursor(2, 1).a('│')
            (2 until center).forEach { ansi.a(' ') }
            ansi.color(DARK_GRAY).a(name).reset()
            ansi.color(LIGHT_GRAY).a(" (").a(closed).a(')').reset()
            (length + center until CONSOLE_WIDTH).forEach { ansi.a(' ') }
            ansi.a('│')
        }
        //Bottom border
        ansi.cursor(3, 1)
        ansi.a('├')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┤')
    }

    override fun drawLocationInfoGeneral(location: Location, page: Int, totalPages: Int, statusMessage: StatusMessage, actions: ActionList) {
        drawLocationTopPanel(location)
        var currentY = 4

        //Page title
        ansi.cursor(currentY, 1)
        val generalInfo = loadString("general_info")
        val pagesText = if (totalPages > 1) " ($page/$totalPages)" else ""
        ansi.a("│ ")
        ansi.a(generalInfo.toUpperCase()).a(pagesText).a(':')
        (3 + generalInfo.length + pagesText.length + 1 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')
        currentY++

        //Blank line
        drawBlankLine(currentY)
        currentY++

        if (page == 1) {
            //Bag size
            ansi.cursor(currentY, 1)
            ansi.a("│ ")
            val bagSizeTitle = loadString("bag_size")
            val bagSizeValue = location.bag.size.toString()
            ansi.a(bagSizeTitle).a(": ")
            ansi.color(LIGHT_YELLOW).a(bagSizeValue).reset()
            (3 + bagSizeTitle.length + 2 + bagSizeValue.length until CONSOLE_WIDTH).forEach { ansi.a(' ') }
            ansi.a('│')
            currentY++

            //Closing difficulty
            ansi.cursor(currentY, 1)
            ansi.a("│ ")
            val closingDifficultyTitle = loadString("closing_difficulty")
            val closingDifficultyValue = location.closingDifficulty.toString()
            ansi.a(closingDifficultyTitle).a(": ")
            ansi.color(LIGHT_YELLOW).a(closingDifficultyValue).reset()
            (3 + closingDifficultyTitle.length + 2 + closingDifficultyValue.length until CONSOLE_WIDTH).forEach { ansi.a(' ') }
            ansi.a('│')
            currentY++

            //Blank line
            drawBlankLine(currentY)
            currentY++
        }

        //Description
        val pageHeight = CONSOLE_HEIGHT - 5 /*top*/ - 4/*bottom*/
        val startLine = when (page) {
            1 -> 1
            else -> (pageHeight - 3) + (page - 2) * pageHeight + 1
        }
        val endLine = when (page) {
            1 -> pageHeight - 3
            else -> (pageHeight - 3) + (page - 1) * pageHeight
        }
        val description = location.description[language]
        currentY += drawMultilineText(currentY, description, true, startLine, endLine)

        //Blank lines
        (currentY until CONSOLE_HEIGHT - 3).forEach { drawBlankLine(it) }

        //Separator
        ansi.cursor(CONSOLE_HEIGHT - 3, 1)
        ansi.a('├')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┤')

        //Status and actions
        drawStatusMessage(CONSOLE_HEIGHT - 2, statusMessage)
        drawActionList(CONSOLE_HEIGHT - 1, actions)

        //Bottom border
        ansi.cursor(CONSOLE_HEIGHT, 1)
        ansi.a('└')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┘')

        //Finalize
        render()
    }

    override fun calculateLocationInfoGeneralPageCount(location: Location): Int {
        val pageHeight = CONSOLE_HEIGHT - 5 /*top*/ - 4 /*bottom*/
        return with(calculateMultilineTextHeight(location.description[language]) - (pageHeight - 3)) {
            when {
                (this > 0) -> 1 + this / pageHeight + if (this % pageHeight > 0) 1 else 0
                else -> 1
            }
        }
    }

    override fun calculateLocationInfoRulesPageCount(location: Location) =
            calculateGenericInfoListPageCount(location.getSpecialRules(), stringHelper::loadSpecialRuleDescription)

    override fun drawLocationInfoRules(location: Location, page: Int, totalPages: Int, statusMessage: StatusMessage, actions: ActionList) =
            drawGenericInfoList({ drawLocationTopPanel(location) }, loadString("special_rules"), location.getSpecialRules(),
                    stringHelper::loadSpecialRuleName, null, stringHelper::loadSpecialRuleDescription, page, totalPages, statusMessage, actions)

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Draws top panel with hero name
     * @param hero Hero to draw
     */
    private fun drawHeroTopPanel(hero: Hero) {
        //Top border
        ansi.cursor(1, 1)
        ansi.a('┌')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┐')
        //Center row
        val name = hero.name.toUpperCase()
        if (hero.isAlive) {
            drawCenteredCaption(2, name, heroColors.getValue(hero.type), true)
        } else {
            val dead = loadString("dead").toUpperCase()
            val length = name.length + 2 + dead.length + 1
            val center = (CONSOLE_WIDTH - length) / 2
            ansi.cursor(2, 1).a('│')
            (2 until center).forEach { ansi.a(' ') }
            ansi.color(DARK_GRAY).a(name).reset()
            ansi.color(LIGHT_RED).a(" ($dead)").reset()
            (length + center until CONSOLE_WIDTH).forEach { ansi.a(' ') }
            ansi.a('│')
        }
        //Bottom border
        ansi.cursor(3, 1)
        ansi.a('├')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┤')
    }

    override fun drawHeroInfoGeneral(hero: Hero, statusMessage: StatusMessage, actions: ActionList) {
        drawHeroTopPanel(hero)
        var currentY = 4

        //Page title
        ansi.cursor(currentY, 1)
        val generalInfo = loadString("general_info")
        ansi.a("│ ")
        ansi.a(generalInfo.toUpperCase()).a(':')
        (3 + generalInfo.length + 1 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')
        currentY++

        //Blank line
        drawBlankLine(currentY)
        currentY++

        //Class
        ansi.cursor(currentY, 1)
        ansi.a("│ ")
        val classTitle = loadString("class")
        val classValue = stringHelper.loadHeroClassName(hero.type).toUpperCase()
        ansi.a(classTitle).a(": ")
        ansi.color(heroColors[hero.type]).a(classValue).reset()
        (3 + classTitle.length + 2 + classValue.length until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')
        currentY++

        //Description
        val description = stringHelper.loadHeroClassDescription(hero.type)
        currentY += drawMultilineText(currentY, description)

        //Blank line
        drawBlankLine(currentY)
        currentY++

        //Hand capacity
        ansi.cursor(currentY, 1)
        ansi.a("│ ")
        val handCapacity = loadString("hand_capacity")
        ansi.a(handCapacity).a(": ")
        ansi.color(LIGHT_YELLOW).a(hero.hand.capacity).reset()
        (3 + handCapacity.length + 2 + 1 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')
        currentY++

        //Blank line
        drawBlankLine(currentY)
        currentY++

        //Dice lists
        //Hand
        currentY += drawDiceList(currentY, loadString("hand"), hero.hand.examine())
        //Bag
        currentY += drawDiceList(currentY, loadString("bag"), hero.bag.examine())
        //Discard
        currentY += drawDiceList(currentY, loadString("discard"), hero.discardPile.examine())

        //Blank lines
        (currentY until CONSOLE_HEIGHT - 3).forEach { drawBlankLine(it) }

        //Separator
        ansi.cursor(CONSOLE_HEIGHT - 3, 1)
        ansi.a('├')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┤')

        //Status and actions
        drawStatusMessage(CONSOLE_HEIGHT - 2, statusMessage)
        drawActionList(CONSOLE_HEIGHT - 1, actions)

        //Bottom border
        ansi.cursor(CONSOLE_HEIGHT, 1)
        ansi.a('└')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┘')

        //Finalize
        render()
    }

    override fun drawHeroInfoDice(hero: Hero, statusMessage: StatusMessage, actions: ActionList) {
        drawHeroTopPanel(hero)
        var currentY = 4

        //Page title
        ansi.cursor(currentY, 1)
        val diceLimits = loadString("dice_limits")
        ansi.a("│ ")
        ansi.a(diceLimits.toUpperCase()).a(':')
        (3 + diceLimits.length + 1 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')
        currentY++

        //Blank lines
        (currentY..currentY + 1).forEach { drawBlankLine(it) }
        currentY += 2

        //Limits
        val limits = hero.getDiceLimits()
        val titles = limits.map { stringHelper.loadDieType(it.type).toUpperCase() }
        val maxTitleLength = (titles.map { it.length }.max() ?: 0) + 5
        val maxLimitLength = (limits.map { it.maximal }.max() ?: 0) * 4 + 4

        //Main loop
        for (index in titles.indices) {
            val limit = limits[index]
            val title = titles[index]
            var currentX = 1
            //Title
            ansi.a("│   ")
            currentX += 4
            ansi.color(dieColors[limit.type]).a(title).reset().a(':')
            (title.length + 1 until maxTitleLength).forEach { ansi.a(' ') }
            currentX += maxTitleLength
            //Limit
            ansi.background(DARK_CYAN).color(LIGHT_YELLOW)
            (1 until limit.current).forEach { ansi.a("    ") }
            ansi.a(String.format("%4d", limit.current)).reset()
            //Reserve
            ansi.background(LIGHT_GRAY).color(BLACK)
            (limit.current + 1..limit.maximal).forEach { ansi.a(String.format("%+4d", it - limit.initial)) }
            (limit.maximal * 4 until maxLimitLength).forEach { ansi.a(' ') }
            currentX += maxLimitLength
            ansi.reset()
            (currentX until CONSOLE_WIDTH).forEach { ansi.a(' ') }
            ansi.a('│')
            //Blank line
            currentY++
            drawBlankLine(currentY)
            currentY++
        }

        //Blank line
        drawBlankLine(currentY)
        currentY++

        //Favored type
        ansi.cursor(currentY, 1)
        ansi.a("│ ")
        val favoredDieType = loadString("favored_die_type")
        val type = stringHelper.loadDieType(hero.favoredDieType).toUpperCase()
        ansi.a(favoredDieType).a(' ')
        ansi.color(dieColors[hero.favoredDieType])
        ansi.a(type).reset()
        (3 + favoredDieType.length + 1 + type.length until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')
        currentY++

        //Blank lines
        (currentY until CONSOLE_HEIGHT - 3).forEach { drawBlankLine(it) }

        //Separator
        ansi.cursor(CONSOLE_HEIGHT - 3, 1)
        ansi.a('├')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┤')

        //Status and actions
        drawStatusMessage(CONSOLE_HEIGHT - 2, statusMessage)
        drawActionList(CONSOLE_HEIGHT - 1, actions)

        //Bottom border
        ansi.cursor(CONSOLE_HEIGHT, 1)
        ansi.a('└')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┘')

        //Finalize
        render()
    }

    override fun drawHeroInfoSkills(hero: Hero, page: Int, totalPages: Int, statusMessage: StatusMessage, actions: ActionList) =
            drawGenericInfoList({ drawHeroTopPanel(hero) }, loadString("skills_traits"), hero.getSkills(), stringHelper::loadSkillName,
                    { skill ->
                        val lvl = loadString("lvl")
                        ansi.a(" (").a(lvl).a('.')
                        ansi.color(LIGHT_YELLOW).a(skill.level).reset()
                        ansi.a('/').a(skill.maxLevel).a(')')
                        2 + lvl.length + 5
                    },
                    { stringHelper.loadSkillDescription(it, it.level) }, page, totalPages, statusMessage, actions)

    override fun calculateHeroInfoSkillsPageCount(hero: Hero) =
            calculateGenericInfoListPageCount(hero.getSkills()) { stringHelper.loadSkillDescription(it, it.level) }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Method for drawing info of an [Obstacle]
     * @param obstacle Obstacle to draw
     * @param statusMessage Status message
     * @param actions List of actions
     */
    override fun drawObstacleInfo(obstacle: Obstacle, statusMessage: StatusMessage, actions: ActionList) {
        //Setup
        val name = obstacle.name[language]
        val description = obstacle.description[language]
        val traitDescriptions = obstacle.getTraits().map { stringHelper.loadTraitDescription(it, name) }
        val diceDescription = String.format(loadString("obstacle_dice_tier${obstacle.tier}"), *obstacle.dieTypes.map { stringHelper.loadDieType(it) }.toTypedArray())
        val descriptionHeight = calculateMultilineTextHeight(description) + 1 + calculateMultilineTextHeight(diceDescription)
        var traitDescriptionsHeight = traitDescriptions.map { calculateMultilineTextHeight(it) }.sum()
        if (traitDescriptionsHeight > 0) {
            traitDescriptionsHeight++
        }
        val contentHeight = 5 + descriptionHeight + traitDescriptionsHeight
        var currentY = max(1, (CONSOLE_HEIGHT - contentHeight) / 2)
        val color = dieColors.getValue(Die.Type.OBSTACLE)

        //Fill blank space
        (1 until currentY).forEach { drawBlankLine(it, false) }

        //Top frame
        ansi.cursor(currentY++, 1)
        ansi.a('┌')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┐')

        //Obstacle name
        val tier = loadString("tier")
        ansi.cursor(currentY++, 1)
        ansi.a("│ ")
        ansi.color(color).a(name.toUpperCase()).reset()
        ansi.a(" ($tier ${obstacle.tier})")
        (3 + name.length + 2 + tier.length + 3 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')
        //Blank line
        drawBlankLine(currentY++)

        //Description
        currentY += drawMultilineText(currentY, description)

        //Obstacle traits
        if (traitDescriptionsHeight > 0) {
            drawBlankLine(currentY++)
            traitDescriptions.forEach { currentY += drawMultilineText(currentY, it, true, -1, -1, (name.toRegex() to color)) }
        }

        //Obstacle dice
        drawBlankLine(currentY++)
        currentY += drawMultilineText(currentY, diceDescription)

        //Bottom frame
        ansi.cursor(currentY++, 1)
        ansi.a('└')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┘')

        //Fill blank space
        (currentY until CONSOLE_HEIGHT).forEach { drawBlankLine(it, false) }

        //Status and actions
        drawActionList(CONSOLE_HEIGHT, actions, false)

        //Finalize
        render()
    }

    override fun drawEnemyInfo(enemy: Enemy, statusMessage: StatusMessage, actions: ActionList) {
        //Setup
        val name = enemy.name[language]
        val description = enemy.description[language]
        val color = dieColors.getValue(Die.Type.ENEMY)
        val traitDescriptions = enemy.getTraits().map { stringHelper.loadTraitDescription(it, name) }
        val descriptionHeight = calculateMultilineTextHeight(description)
        var traitDescriptionsHeight = traitDescriptions.map { calculateMultilineTextHeight(it) }.sum()
        if (traitDescriptionsHeight > 0) {
            traitDescriptionsHeight++
        }
        val contentHeight = 5 + descriptionHeight + traitDescriptionsHeight
        var currentY = max(1, (CONSOLE_HEIGHT - contentHeight) / 2)

        //Fill blank space
        (1 until currentY).forEach { drawBlankLine(it, false) }

        //Top frame
        ansi.cursor(currentY++, 1)
        ansi.a('┌')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┐')

        //Enemy name
        ansi.cursor(currentY++, 1)
        ansi.a("│ ")
        ansi.color(color).a(name.toUpperCase()).reset()
        (3 + name.length until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')

        //Blank line
        drawBlankLine(currentY++)

        //Description
        currentY += drawMultilineText(currentY, description)

        //Threat traits
        if (traitDescriptionsHeight > 0) {
            drawBlankLine(currentY++)
            traitDescriptions.forEach { currentY += drawMultilineText(currentY, it, true, -1, -1, (name.toRegex() to color)) }
        }

        //Bottom frame
        ansi.cursor(currentY++, 1)
        ansi.a('└')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┘')

        //Fill blank space
        (currentY until CONSOLE_HEIGHT).forEach { drawBlankLine(it, false) }

        //Status and actions
        drawActionList(CONSOLE_HEIGHT, actions, false)

        //Finalize
        render()
    }

    override fun drawVillainInfo(villain: Villain, page: Int, totalPages: Int, statusMessage: StatusMessage, actions: ActionList) {
        //Setup
        val name = villain.name[language]
        val description = villain.description[language] + if (villain.getTraits().isNotEmpty()) {
            "\n\n" + villain.getTraits().joinToString("\n") { stringHelper.loadTraitDescription(it, name) }
        } else ""
        val color = dieColors.getValue(Die.Type.VILLAIN)
        var currentY = 1

        //Top border
        ansi.cursor(currentY++, 1)
        ansi.a('┌')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┐')
        //Villain name
        drawCenteredCaption(currentY++, name.toUpperCase(), color)
        //Bottom border
        ansi.cursor(currentY++, 1)
        ansi.a('├')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┤')

        //Page title
        ansi.cursor(currentY, 1)
        val generalInfo = loadString("general_info")
        val pagesText = if (totalPages > 1) " ($page/$totalPages)" else ""
        ansi.a("│ ")
        ansi.a(generalInfo.toUpperCase()).a(pagesText).a(':')
        (3 + generalInfo.length + pagesText.length + 1 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')
        currentY++

        //Blank line
        drawBlankLine(currentY)
        currentY++

        //Description
        val pageHeight = CONSOLE_HEIGHT - 5 /*top*/ - 4/*bottom*/
        val startLine = pageHeight * (page - 1) + 1
        val endLine = pageHeight * page
        currentY += drawMultilineText(currentY, description, true, startLine, endLine, (name.toRegex() to color))

        //Blank lines
        (currentY until CONSOLE_HEIGHT - 3).forEach { drawBlankLine(it) }

        //Separator
        ansi.cursor(CONSOLE_HEIGHT - 3, 1)
        ansi.a('├')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┤')

        //Status and actions
        drawStatusMessage(CONSOLE_HEIGHT - 2, statusMessage)
        drawActionList(CONSOLE_HEIGHT - 1, actions)

        //Bottom border
        ansi.cursor(CONSOLE_HEIGHT, 1)
        ansi.a('└')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┘')

        //Finalize
        render()
    }

    override fun calculateVillainInfoPageCount(villain: Villain): Int {
        val pageHeight = CONSOLE_HEIGHT - 5 /*top*/ - 4 /*bottom*/
        val name = villain.name[language]
        val textHeight = calculateMultilineTextHeight(villain.description[language]) +
                villain.getTraits()
                        .map { calculateMultilineTextHeight(stringHelper.loadTraitDescription(it, name)) }
                        .sum() + 1
        return with(textHeight - pageHeight) {
            1 + this / pageHeight + if (this % pageHeight > 0) 1 else 0
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    override fun drawModifiersInfo(hero: Hero, skillModifiers: List<PendingSkillModifier>, allyModifiers: List<PendingAllyModifier>, statusMessage: StatusMessage, actions: ActionList) {
        //Calculations
        val skillNames = skillModifiers.map { stringHelper.loadSkillName(it.type).toUpperCase() }
        val allySkillNames = allyModifiers.map { stringHelper.loadAllySkillName(it.type).toUpperCase() }
        val contentHeight = skillModifiers.size + allyModifiers.size + 2
        val namesWidth = (skillNames.map { it.length } + allySkillNames.map { it.length }).max() ?: 0
        val valuesSum = (skillModifiers.map { it.value } + allyModifiers.map { it.value }).sum()
        val totalValue = String.format("%+d", valuesSum)
        val valuesWidth = max(totalValue.length,
                (skillModifiers.map { String.format("%+d", it.value).length } + allyModifiers.map { String.format("%+d", it.value).length }).max()
                        ?: 0)
        val contentWidth = 1 + namesWidth + 2 + valuesWidth + 1
        val offsetY = (CONSOLE_HEIGHT - contentHeight) / 2
        val offsetX = (CONSOLE_WIDTH - contentWidth) / 2

        //Fill blank space
        (1 until offsetY - 2).forEach { drawBlankLine(it, false) }

        //Title
        ansi.cursor(offsetY - 2, offsetX + 1)
        ansi.eraseLine(Ansi.Erase.BACKWARD)
        ansi.a(loadString("modifiers") + ":")
        ansi.eraseLine(Ansi.Erase.FORWARD)

        //Top frame
        ansi.cursor(offsetY - 1, offsetX - 1)
        ansi.eraseLine(Ansi.Erase.BACKWARD)
        ansi.a('╔')
        (0 until contentWidth).forEach { ansi.a('═') }
        ansi.a('╗')
        ansi.eraseLine(Ansi.Erase.FORWARD)

        //Dialog contents
        var row = 0
        skillModifiers.forEachIndexed { index, skill ->
            ansi.cursor(offsetY + row, offsetX - 1)
            ansi.eraseLine(Ansi.Erase.BACKWARD)
            ansi.a("║ ")
            val name = skillNames[index]
            val value = String.format("%+d", skill.value)
            ansi.color(LIGHT_YELLOW)
            ansi.a(name).reset()
            (name.length until namesWidth).forEach { ansi.a(' ') }
            ansi.a(": ")
            ansi.color(if (skill.value >= 0) LIGHT_GREEN else LIGHT_RED)
            ansi.a(value).reset()
            (value.length until valuesWidth).forEach { ansi.a(' ') }
            ansi.a(" ║")
            ansi.eraseLine(Ansi.Erase.FORWARD)
            row++
        }
        allyModifiers.forEachIndexed { index, ally ->
            ansi.cursor(offsetY + row, offsetX - 1)
            ansi.eraseLine(Ansi.Erase.BACKWARD)
            ansi.a("║ ")
            val name = allySkillNames[index]
            val value = String.format("%+d", ally.value)
            ansi.color(WHITE)
            ansi.a(name).reset()
            (name.length until namesWidth).forEach { ansi.a(' ') }
            ansi.a(": ")
            ansi.color(if (ally.value >= 0) LIGHT_GREEN else LIGHT_RED)
            ansi.a(value).reset()
            (value.length until valuesWidth).forEach { ansi.a(' ') }
            ansi.a(" ║")
            ansi.eraseLine(Ansi.Erase.FORWARD)
            row++
        }

        //Separator line
        ansi.cursor(offsetY + row, offsetX - 1)
        ansi.eraseLine(Ansi.Erase.BACKWARD)
        ansi.a('╟')
        (0 until contentWidth).forEach { ansi.a('─') }
        ansi.a('╢')
        ansi.eraseLine(Ansi.Erase.FORWARD)
        row++

        //Total
        val totalName = loadString("total")
        ansi.cursor(offsetY + row, offsetX - 1)
        ansi.eraseLine(Ansi.Erase.BACKWARD)
        ansi.a("║ ")
        (0 until namesWidth - totalName.length).forEach { ansi.a(' ') }
        ansi.a(totalName).a(": ")
        ansi.color(if (valuesSum >= 0) LIGHT_GREEN else LIGHT_RED)
        ansi.a(totalValue).reset()
        (totalValue.length until valuesWidth).forEach { ansi.a(' ') }
        ansi.a(" ║")
        ansi.eraseLine(Ansi.Erase.FORWARD)

        //Bottom frame
        ansi.cursor(offsetY + contentHeight, offsetX - 1)
        ansi.eraseLine(Ansi.Erase.BACKWARD)
        ansi.a('╚')
        (0 until contentWidth).forEach { ansi.a('═') }
        ansi.a('╝')
        ansi.eraseLine(Ansi.Erase.FORWARD)

        //Fill blank space
        (offsetY + contentHeight + 1 until CONSOLE_HEIGHT - 1).forEach { drawBlankLine(it, false) }

        //Status and actions
        drawStatusMessage(CONSOLE_HEIGHT - 1, statusMessage, false)
        drawActionList(CONSOLE_HEIGHT, actions, false)

        //Finalize
        render()
    }

    //------------------------------------------------------------------------------------------------------------------
    override fun drawGameVictory(message: StatusMessage) {
        val offsetY = CONSOLE_HEIGHT / 2
        (1 until offsetY).forEach { drawBlankLine(it, false) }
        val data = stringHelper.loadStatusMessage(message).toUpperCase()
        drawCenteredCaption(offsetY, data, LIGHT_GREEN, false)
        (offsetY + 1..CONSOLE_HEIGHT).forEach { drawBlankLine(it, false) }
        render()
    }

    //------------------------------------------------------------------------------------------------------------------
    override fun drawGameLoss(message: StatusMessage) {
        val offsetY = CONSOLE_HEIGHT / 2
        (1 until offsetY).forEach { drawBlankLine(it, false) }
        val data = stringHelper.loadStatusMessage(message).toUpperCase()
        drawCenteredCaption(offsetY, data, LIGHT_RED, false)
        (offsetY + 1..CONSOLE_HEIGHT).forEach { drawBlankLine(it, false) }
        render()
    }

    //------------------------------------------------------------------------------------------------------------------
    override fun drawGamePauseMenu(statusMessage: StatusMessage, actions: ActionList) {
        val serviceActions = setOf(Action.Type.CONFIRM, Action.Type.CANCEL)
        val actions1 = (0 until actions.size).map { actions[it] }.filter { it.type !in serviceActions }.fold(ActionList()) { al, a -> al.add(a) }
        val actions2 = (0 until actions.size).map { actions[it] }.filter { it.type in serviceActions }.fold(ActionList()) { al, a -> al.add(a) }
        val contentHeight = actions1.size * 2
        val contentWidth = (0 until actions1.size).asSequence()
                .map { actions[it] }
                .map { a -> stringHelper.loadActionData(a).sumBy { it.length } + 6 }
                .max() ?: 0
        val offsetX = (CONSOLE_WIDTH - contentWidth) / 2
        val offsetY = (CONSOLE_HEIGHT - contentHeight) / 2

        //Fill blank space
        (1 until offsetY).forEach { drawBlankLine(it, false) }

        //Top actions
        if (actions1.size > 0) {
            //Top frame
            ansi.cursor(offsetY, offsetX)
            ansi.eraseLine(Ansi.Erase.BACKWARD)
            ansi.a('╔')
            (0 until contentWidth).forEach { ansi.a('═') }
            ansi.a('╗')
            ansi.eraseLine(Ansi.Erase.FORWARD)
            //Actions
            actions1.forEachIndexed { index, action ->
                val data = stringHelper.loadActionData(action)
                val name = if (data[1][0].isLowerCase()) data[0] + data[1] else data[1]
                ansi.cursor(offsetY + index * 2 + 1, offsetX)
                ansi.eraseLine(Ansi.Erase.BACKWARD)
                ansi.a('║')
                ansi.color(LIGHT_YELLOW)
                ansi.a(" (${data[0]}) ").reset()
                ansi.a(name.toUpperCase())
                (offsetX + 3 + data[0].length + 1 + name.length until offsetX + contentWidth).forEach { ansi.a(' ') }
                ansi.a('║')
                ansi.eraseLine(Ansi.Erase.FORWARD)
                //Separator
                if (index < actions1.size - 1) {
                    ansi.cursor(offsetY + index * 2 + 2, offsetX)
                    ansi.eraseLine(Ansi.Erase.BACKWARD)
                    ansi.a('╟')
                    (0 until contentWidth).forEach { ansi.a('─') }
                    ansi.a('╢')
                    ansi.eraseLine(Ansi.Erase.FORWARD)
                }
            }
            //Bottom frame
            ansi.cursor(offsetY + contentHeight, offsetX)
            ansi.eraseLine(Ansi.Erase.BACKWARD)
            ansi.a('╚')
            (0 until contentWidth).forEach { ansi.a('═') }
            ansi.a('╝')
            ansi.eraseLine(Ansi.Erase.FORWARD)
        } else {
            drawBlankLine(offsetY, false)
        }

        //Fill blank space
        (offsetY + contentHeight + 1 until CONSOLE_HEIGHT - 1).forEach { drawBlankLine(it, false) }

        //Status and actions
        drawStatusMessage(CONSOLE_HEIGHT - 1, statusMessage, false)
        drawActionList(CONSOLE_HEIGHT, actions2, false)

        //Finalize
        render()
    }

    //------------------------------------------------------------------------------------------------------------------
    override fun drawScenarioScenicText(scenario: Scenario, isOutro: Boolean, page: Int, totalPages: Int, actions: ActionList) {
        var currentY = 1
        if (page == 1) {
            //Page title
            val title = scenario.name[language].toUpperCase()
            drawCenteredCaption(currentY++, title, LIGHT_YELLOW, false)
            //Blank line
            drawBlankLine(currentY++, false)
        }

        //Scenic text
        val pageHeight = CONSOLE_HEIGHT - 2 /*bottom*/
        val startLine = when (page) {
            1 -> 1
            else -> (pageHeight - 2) + (page - 2) * pageHeight + 1
        }
        val endLine = when (page) {
            1 -> pageHeight - 2
            else -> (pageHeight - 2) + (page - 1) * pageHeight
        }
        val text = when (isOutro) {
            false -> scenario.intro[language]
            true -> scenario.outro[language]
        }
        currentY += drawMultilineText(currentY, text, false, startLine, endLine)

        //Blank line
        (currentY until CONSOLE_HEIGHT - 1).forEach { drawBlankLine(it, false) }
        drawBlankLine(CONSOLE_HEIGHT - 1, false)

        //Actions
        drawActionList(CONSOLE_HEIGHT, actions, false)

        //Finalize
        render()
    }

    override fun calculateScenarioScenicTextPageCount(scenario: Scenario, isOutro: Boolean): Int {
        val text = when (isOutro) {
            false -> scenario.intro[language]
            true -> scenario.outro[language]
        }
        val pageHeight = CONSOLE_HEIGHT - 2 /*bottom*/
        return with(calculateMultilineTextHeight(text, false) - (pageHeight - 2)) {
            when {
                (this > 0) -> 1 + this / pageHeight + if (this % pageHeight > 0) 1 else 0
                else -> 1
            }
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Draw ordered list of dice
     * @param offsetY Vertical offset
     * @param title LIst title
     * @param dice List of dice
     * @return number of lines needed to draw the list
     */
    private fun drawDiceList(offsetY: Int, title: String, dice: List<Die>): Int {
        var lineCount = 0
        val empty = " [${loadString("empty").toLowerCase()}]"
        ansi.cursor(offsetY, 1)
        ansi.a("│ ")
        ansi.a(title).a(':')
        (3 + title.length + 1 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')
        lineCount++
        ansi.cursor(offsetY + lineCount, 1)
        ansi.a('│')
        var currentX = 2
        dice.forEach { die ->
            val textLength = if (die.size >= 10) 4 else 3
            if (currentX + textLength >= CONSOLE_WIDTH) { //Need to wrap to next line
                (currentX until CONSOLE_WIDTH).forEach { ansi.a(' ') }
                ansi.a('│')
                lineCount++
                ansi.cursor(offsetY + lineCount, 1)
                ansi.a('│')
                currentX = 2
            }
            ansi.a(' ')
            drawDieSize(die)
            currentX += textLength
        }
        if (currentX == 2) { //No dice
            ansi.color(DARK_GRAY).a(empty).reset()
            currentX += empty.length
        }
        (currentX until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')

        return ++lineCount
    }
}