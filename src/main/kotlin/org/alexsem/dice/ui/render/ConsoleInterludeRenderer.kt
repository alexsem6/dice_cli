package org.alexsem.dice.ui.render

import org.alexsem.dice.ui.render.strings.StringLoader
import org.alexsem.dice.model.Interlude

class ConsoleInterludeRenderer(loader: StringLoader) : ConsoleRenderer(loader), InterludeRenderer {

    private val npcColors = mutableListOf(
            Color.WHITE,
            Color.DARK_MAGENTA,
            Color.DARK_CYAN,
            Color.DARK_RED,
            Color.DARK_GREEN,
            Color.DARK_YELLOW,
            Color.LIGHT_RED
    ).apply { shuffle() }

    //------------------------------------------------------------------------------------------------------------------

    override fun drawHeadline(interlude: Interlude, headline: Interlude.Event.Headline) {
        //Calculations
        val text = "  " + headline.text[language]
        val offsetY = (CONSOLE_HEIGHT - 1) / 2

        //Blank lines
        (1 until offsetY).forEach { y -> drawBlankLine(y, false) }

        //Caption text
        drawCenteredCaption(offsetY, text, Color.LIGHT_YELLOW, false)

        //Blank lines
        (offsetY + 1..CONSOLE_HEIGHT).forEach { y -> drawBlankLine(y, false) }

        //Finalize
        render()
    }

    //------------------------------------------------------------------------------------------------------------------

    override fun drawNarration(interlude: Interlude, text: Interlude.Event.Narration) {
        //Calculations
        val narration = "  " + text.text[language]
        val textHeight = calculateMultilineTextHeight(narration, false)
        val offsetY = (CONSOLE_HEIGHT - textHeight) / 2

        //Blank lines
        (1 until offsetY).forEach { y -> drawBlankLine(y, false) }

        //Caption text
        val height = drawMultilineCenteredText(offsetY, narration, false)

        //Blank lines
        (offsetY + height..CONSOLE_HEIGHT).forEach { y -> drawBlankLine(y, false) }

        //Finalize
        render()
    }

    //------------------------------------------------------------------------------------------------------------------

    override fun drawPhrase(interlude: Interlude, phrase: Interlude.Event.Phrase) {
        //Calculations
        val actor = phrase.actor
        val actorName = when (actor) {
            is Interlude.Actor.Hero -> actor.hero.name
            is Interlude.Actor.NPC -> actor.name[language]
        }.toUpperCase() + ": "
        val actorColor = when (actor) {
            is Interlude.Actor.Hero -> heroColors.getValue(actor.hero.type)
            is Interlude.Actor.NPC -> npcColors[actor.index % npcColors.size]
        }
        val text = actorName + phrase.text[language]
        val textHeight = calculateMultilineTextHeight(text, false)
        val offsetY = CONSOLE_HEIGHT - textHeight + 1

        //Blank lines
        (1 until offsetY).forEach { y -> drawBlankLine(y, false) }

        //Speech text
        drawMultilineText(offsetY, text, false, -1, -1, actorName.toRegex() to actorColor)

        //Finalize
        render()
    }
}