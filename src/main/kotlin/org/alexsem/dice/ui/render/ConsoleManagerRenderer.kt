package org.alexsem.dice.ui.render

import org.alexsem.dice.ui.ActionList
import org.alexsem.dice.ui.StatusMessage
import org.alexsem.dice.ui.render.strings.StringLoader
import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Hero
import org.fusesource.jansi.Ansi
import kotlin.math.max
import kotlin.math.min

class ConsoleManagerRenderer(loader: StringLoader) : ConsoleRenderer(loader), ManagerRenderer {

    override val maxHeroNameLength = (CONSOLE_WIDTH - 20) / 5
    override val maxPartyNameLength = CONSOLE_WIDTH / 4
    override val commonPoolDicePerPage = 10

    override fun shortcut(index: Int) = "1234567890-="[index]

    /**
     * Draws top panel with hero name
     * @param hero Hero to draw
     */
    private fun drawHeroTopPanel(hero: Hero) {
        //Top border
        ansi.cursor(1, 1)
        ansi.a('┌')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┐')
        //Center row
        val name = hero.name.toUpperCase()
        val length = name.length + 6
        val center = (CONSOLE_WIDTH - length) / 2
        ansi.cursor(2, 1).a('│')
        (2 until center).forEach { ansi.a(' ') }
        ansi.color(Color.LIGHT_YELLOW).a("<  ").reset()
        ansi.color(heroColors.getValue(hero.type)).a(name).reset()
        ansi.color(Color.LIGHT_YELLOW).a("  >").reset()
        (length + center until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')
        //Bottom border
        ansi.cursor(3, 1)
        ansi.a('├')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┤')
    }

    /**
     * Draws one specific page of common pile on console
     * @param offsetY         Line to begin input on (1-based)
     * @param pile            Pile (subpile) to draw
     * @param activePositions List of position numbers to be displayed as active
     * @param displayButtons false to hide button row entirely
     */
    private fun drawCommonPool(offsetY: Int, pile: List<Die>, activePositions: Set<Int>, displayButtons: Boolean = true) {
        val size = pile.size
        val slots = max(size, commonPoolDicePerPage)
        var currentX = 1

        //Left border
        ansi.cursor(offsetY, currentX)
        ansi.a("│  ┌")
        ansi.cursor(offsetY + 1, currentX)
        ansi.a("│  │")
        ansi.cursor(offsetY + 2, currentX)
        ansi.a("│  └")
        ansi.cursor(offsetY + 3, currentX)
        ansi.a("│   ")
        currentX += 4

        //Dice in pile
        for (i in 0 until slots) {
            val die = if (i < size) pile[i] else null
            val longDieName = die != null && die.size >= 10

            //Top border
            ansi.cursor(offsetY, currentX)
            ansi.a("────").a(if (longDieName) "─" else "")
            ansi.a(if (i < slots - 1) '┬' else '┐')

            //Center row
            ansi.cursor(offsetY + 1, currentX)
            ansi.a(' ')
            if (die != null) {
                ansi.color(dieColors[die.type])
                ansi.a(die.toString()).reset()
            } else {
                ansi.a("  ")
            }
            ansi.a(' ')
            ansi.a('│')

            //Bottom border
            ansi.cursor(offsetY + 2, currentX)
            ansi.a("────").a(if (longDieName) '─' else "")
            ansi.a(if (i < slots - 1) '┴' else '┘')

            //Die number
            ansi.cursor(offsetY + 3, currentX)
            if (i in activePositions) {
                ansi.color(Color.LIGHT_YELLOW)
            }
            when {
                !displayButtons -> ansi.a("     " + if (longDieName) " " else "")
                i < size -> ansi.a(" (${(i + 1) % 10}) " + if (longDieName) " " else "")
                else -> ansi.a("     ")
            }
            ansi.reset()
            currentX += 5 + if (longDieName) 1 else 0
        }

        //Clear the end of the line
        (0..3).forEach { row ->
            ansi.cursor(offsetY + row, currentX)
            for (it in currentX until CONSOLE_WIDTH) {
                ansi.a(' ')
            }
            ansi.a('│')
        }

    }

    override fun drawDiceManagerScreen(hero: Hero, heroDice: Map<Die.Type, List<Die>>, pickedType: Die.Type?, commonPool: List<Die>, page: Int, totalPages: Int, activePositions: Set<Int>, statusMessage: StatusMessage, actions: ActionList) {
        drawHeroTopPanel(hero)
        var currentY = 4

        //Hero bag title
        ansi.cursor(currentY, 1)
        val diceTitle = loadString("hero_bag")
        ansi.a("│ ")
        ansi.a(diceTitle.toUpperCase()).a(':')
        (3 + diceTitle.length + 1 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')
        currentY++

        //Hero dice
        val separatorX1 = 4 + (heroDice.keys.asSequence()
                .map(stringHelper::loadDieType)
                .map(String::length)
                .max() ?: 0) + 1
        val separatorX2 = separatorX1 + 6
        //Blank line
        ansi.cursor(currentY, 1)
        ansi.a('│')
        (2 until separatorX1).forEach { ansi.a(' ') }
        ansi.a("│   │ ")
        (separatorX2 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')
        currentY++
        //Dice in bag
        hero.getDiceLimits().forEach { l ->
            //Type names
            val dice = heroDice[l.type] ?: listOf()
            val type = stringHelper.loadDieType(l.type).toUpperCase()
            ansi.cursor(currentY, 1)
            ansi.a("│  ")
            ansi.color(dieColors[l.type]).a(type).reset()
            var currentX = 4 + type.length
            (currentX until separatorX1).forEach { ansi.a(' ') }
            //Current limits
            ansi.a("│ ${l.current}" + if (l.current < 10) " │ " else "│ ")
            currentX = separatorX2
            //Dice
            dice.forEach { d ->
                ansi.color(dieColors[l.type])
                ansi.a(" $d " + if (d.size < 10) " " else "").reset()
                currentX += 5
            }
            //Blank die spaces
            val spareDiceInCommonPool = commonPool.any { it.type == l.type }
            (dice.size until l.current).forEach {
                ansi.a(' ')
                ansi.background(Color.DARK_RED)
                ansi.color(Color.BLACK)
                ansi.a(if (spareDiceInCommonPool) "   " else "d4 ").reset()
                ansi.a(' ')
                currentX += 5
            }
            (currentX until CONSOLE_WIDTH).forEach { ansi.a(' ') }
            ansi.a('│')
            currentY++
            //Buttons
            ansi.cursor(currentY, 1)
            ansi.a('│')
            (2 until separatorX1).forEach { ansi.a(' ') }
            ansi.a("│   │ ")
            currentX = separatorX2
            if (l.type == pickedType) {
                ansi.color(Color.LIGHT_YELLOW)
                (0 until dice.size).forEach {
                    ansi.a(" (${shortcut(it)}) ")
                    currentX++
                }
                ansi.reset()
            }
            (currentX until CONSOLE_WIDTH).forEach { ansi.a(' ') }
            ansi.a('│')
            currentY++
        }

        //Separator
        ansi.cursor(currentY, 1)
        ansi.a('├')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┤')
        currentY++

        //Common pool
        ansi.cursor(currentY, 1)
        val pileTitle = loadString("common_pile")
        val pagesText = " ($page/$totalPages)"
        ansi.a("│ ")
        ansi.a(pileTitle.toUpperCase()).a(pagesText).a(':')
        (3 + pileTitle.length + pagesText.length + 1 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')
        currentY++

        //Common pile
        val range = (page - 1) * commonPoolDicePerPage until min(page * commonPoolDicePerPage, commonPool.size)
        drawCommonPool(currentY,
                commonPool.subList(range.start, range.endInclusive + 1),
                when (pickedType == null) {
                    true -> activePositions.asSequence()
                            .filter { it in range }
                            .map { it - range.start }
                            .toSet()
                    false -> setOf()
                }, (pickedType == null))
        currentY += 4

        //Blank lines
        val separatorY = CONSOLE_HEIGHT - 2 - 2
        (currentY until separatorY).forEach { drawBlankLine(it) }

        //Separator
        ansi.cursor(separatorY, 1)
        ansi.a('├')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┤')

        //Status and actions
        drawStatusMessage(separatorY + 1, statusMessage)
        drawActionList(separatorY + 2, actions)

        //Blank lines
        currentY = separatorY + 2 + calculateActionListHeight(actions)
        (currentY until CONSOLE_HEIGHT).forEach { drawBlankLine(it) }

        //Bottom border
        ansi.cursor(CONSOLE_HEIGHT, 1)
        ansi.a('└')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┘')

        //Finalize
        render()
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Draws top panel with party name
     * @param partyName Name of the party
     */
    private fun drawPartyTopPanel(partyName: String) {
        //Top border
        ansi.cursor(1, 1)
        ansi.a('┌')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┐')
        //Center row
        drawCenteredCaption(2, partyName.toUpperCase(), Color.LIGHT_YELLOW)
        //Bottom border
        ansi.cursor(3, 1)
        ansi.a('├')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┤')
    }

    /**
     * Draw hero image at specified coordinates
     * @param offsetY Vertical position
     * @param offsetX Horizontal position
     * @param width Image width
     * @param type Hero type (class)
     * @param name Hero name (may be blank)
     * @param index Hero index (if needed)
     * @param shadowed true to draw hero dimmed, false for normal colors
     */
    private fun drawHero(offsetY: Int, offsetX: Int, width: Int, type: Hero.Type, name: String, index: Int? = null, shadowed: Boolean = false) {
        val heroOffset = (width - 3) / 2
        val nameOffset = (width - name.length) / 2

        //Helper methods
        fun Ansi.left(): Ansi {
            (0 until heroOffset).forEach { this.a(' ') }
            return this
        }

        fun Ansi.right(): Ansi {
            (heroOffset + 3 until width).forEach { this.a(' ') }
            return this
        }

        //Image
        ansi.color(if (shadowed) Color.DARK_GRAY else heroColors[type])
        ansi.cursor(offsetY, offsetX)
        ansi.left().a(" ☻ ").right()
        ansi.cursor(offsetY + 1, offsetX)
        ansi.left().a("/|\\").right()
        ansi.cursor(offsetY + 2, offsetX)
        ansi.left().a("/ \\").right()
        ansi.reset()
        //Name
        ansi.color(if (shadowed) Color.DARK_GRAY else Color.WHITE)
        ansi.cursor(offsetY + 3, offsetX)
        (0 until nameOffset).forEach { ansi.a(' ') }
        ansi.a(name.toUpperCase())
        (nameOffset + name.length until width).forEach { ansi.a(' ') }
        ansi.reset()
        //Index
        ansi.color(if (shadowed) Color.DARK_GRAY else Color.LIGHT_YELLOW)
        ansi.cursor(offsetY + 4, offsetX)
        ansi.left().a(index?.let { "(${shortcut(it)})" } ?: "   ").right()
        ansi.reset()
    }

    /**
     * Draw list of hero images
     * @param offsetY Vertical offset
     * @param heroes List of heroes
     * @param showIndex true to list hero indexes
     * @param shadowed true to draw heroes dimmed, false for normal colors
     * @return hero list height
     */
    private fun drawHeroes(offsetY: Int, heroes: List<Hero>, showIndex: Boolean = false, shadowed: Boolean = false): Int {
        val width = maxHeroNameLength + 2
        var offsetX = (CONSOLE_WIDTH - width * 5) / 2
        //Left border
        (offsetY..offsetY + 4).forEach { y ->
            ansi.cursor(y, 1)
            ansi.a('│')
            (2 until offsetX).forEach { ansi.a(' ') }
        }
        //Heroes
        heroes.forEachIndexed { i, h ->
            drawHero(offsetY, offsetX, width, h.type, h.name, if (showIndex) i else null, shadowed)
            offsetX += width
        }
        //Right border
        (offsetY..offsetY + 4).forEach { y ->
            ansi.cursor(y, offsetX)
            (offsetX until CONSOLE_WIDTH).forEach { ansi.a(' ') }
            ansi.a('│')
        }
        return 5
    }

    /**
     * Draw list of hero images with one of them standing out from the rest
     * @param offsetY Vertical offset
     * @param heroes List of heroes
     * @param index Index of hero who stands out
     * @param type Type of hero who stands out
     * @param name Name of hero who stands out
     * @return Height of the hero list
     */
    private fun drawSingleHero(offsetY: Int, heroes: List<Hero>, index: Int, type: Hero.Type, name: String): Int {
        val width = maxHeroNameLength + 2
        var offsetX = (CONSOLE_WIDTH - width * 5) / 2
        //Left border
        (offsetY..offsetY + 4).forEach { y ->
            ansi.cursor(y, 1)
            ansi.a('│')
            (2 until offsetX).forEach { ansi.a(' ') }
        }
        //Heroes
        (0..max(heroes.size - 1, index)).forEach { i ->
            when {
                i == index -> drawHero(offsetY, offsetX, width, type, name)
                i < heroes.size -> drawHero(offsetY, offsetX, width, heroes[i].type, heroes[i].name, null, true)
                else -> (offsetY..offsetY + 4).forEach { y ->
                    ansi.cursor(y, offsetX)
                    (0 until width).forEach { ansi.a(' ') }
                }
            }
            offsetX += width
        }
        //Right border
        (offsetY..offsetY + 4).forEach { y ->
            ansi.cursor(y, offsetX)
            (offsetX until CONSOLE_WIDTH).forEach { ansi.a(' ') }
            ansi.a('│')
        }

        return 5
    }

    /**
     * Draw party manager general screen
     * @param partyName Name of current party
     * @param heroesArea Code that fills hero list area (takes offset, returns height)
     * @param specialArea Code that fills bottom area (takes offset, returns height)
     * @param statusMessage Status message
     * @param actions List of actions
     */
    private fun drawPartyManagerScreen(partyName: String, heroesArea: (Int) -> Int, specialArea: (Int) -> Int, statusMessage: StatusMessage, actions: ActionList) {
        //Top panel
        drawPartyTopPanel(partyName)
        var currentY = 4

        //Page title
        ansi.cursor(currentY, 1)
        ansi.a("│ ")
        val title = loadString("heroes")
        ansi.a(title.toUpperCase()).a(':')
        (3 + title.length + 1 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')
        currentY++

        //Blank lines
        (currentY..currentY + 1).forEach { drawBlankLine(it) }
        currentY += 2

        //List of heroes
        currentY += heroesArea.invoke(currentY)

        //Special area
        currentY += specialArea.invoke(currentY)

        //Blank lines
        val separatorY = CONSOLE_HEIGHT - 2 - calculateActionListHeight(actions)
        (currentY until separatorY).forEach { drawBlankLine(it) }

        //Separator
        ansi.cursor(separatorY, 1)
        ansi.a('├')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┤')

        //Status and actions
        drawStatusMessage(separatorY + 1, statusMessage)
        drawActionList(separatorY + 2, actions)

        //Bottom border
        ansi.cursor(CONSOLE_HEIGHT, 1)
        ansi.a('└')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┘')

        //Finalize
        render()
    }

    override fun drawPartyManagerGeneral(partyName: String, heroes: List<Hero>, statusMessage: StatusMessage, actions: ActionList) =
            drawPartyManagerScreen(partyName, { y -> drawHeroes(y, heroes, false) }, { 0 }, statusMessage, actions)

    override fun drawPartyManagerHeroSelection(partyName: String, heroes: List<Hero>, statusMessage: StatusMessage, actions: ActionList) =
            drawPartyManagerScreen(partyName, { y -> drawHeroes(y, heroes, true) }, { 0 }, statusMessage, actions)

    override fun drawHeroClassSelectionDialog(partyName: String, heroes: List<Hero>, classes: List<Hero.Type>, statusMessage: StatusMessage, actions: ActionList) =
            drawPartyManagerScreen(partyName, { y -> drawHeroes(y, heroes, showIndex = false, shadowed = true) }, { y ->
                //Calculations
                val classNames = classes.map { stringHelper.loadHeroClassName(it) }
                val contentWidth = 6 + (classNames.asSequence().map(String::length).max() ?: 0)
                val offsetX = (CONSOLE_WIDTH - contentWidth) / 2 - 1
                var currentY = y
                //Title
                ansi.cursor(currentY, 1)
                ansi.a('│')
                (2 until offsetX + 2).forEach { ansi.a(' ') }
                val chooseClass = loadString("choose_hero_class")
                ansi.a(chooseClass)
                (offsetX + 2 + chooseClass.length until CONSOLE_WIDTH).forEach { ansi.a(' ') }
                ansi.a('│')
                currentY++

                //Top frame
                ansi.cursor(currentY, 1)
                ansi.a('│')
                (2 until offsetX).forEach { ansi.a(' ') }
                ansi.a('╔')
                (0 until contentWidth).forEach { ansi.a('═') }
                ansi.a('╗')
                (offsetX + contentWidth + 2 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
                ansi.a('│')
                currentY++

                //Dialog contents
                classes.forEachIndexed { index, type ->
                    val name = classNames[index]
                    ansi.cursor(currentY, 1)
                    ansi.a('│')
                    (2 until offsetX).forEach { ansi.a(' ') }
                    ansi.a('║')
                    ansi.color(Color.LIGHT_YELLOW).a(" (${index + 1}) ").reset()
                    ansi.color(heroColors[type]).a(name.toUpperCase()).reset()
                    (offsetX + 7 + name.length until offsetX + contentWidth + 2).forEach { ansi.a(' ') }
                    ansi.a('║')
                    (offsetX + contentWidth + 2 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
                    ansi.a('│')
                    currentY++
                }

                //Bottom frame
                ansi.cursor(currentY, 1)
                ansi.a('│')
                (2 until offsetX).forEach { ansi.a(' ') }
                ansi.a('╚')
                (0 until contentWidth).forEach { ansi.a('═') }
                ansi.a('╝')
                (offsetX + contentWidth + 2 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
                ansi.a('│')
                currentY++

                currentY - y
            }, statusMessage, actions)

    override fun drawHeroNameEditScreen(partyName: String, heroes: List<Hero>, heroIndex: Int, heroType: Hero.Type, currentText: String, statusMessage: StatusMessage, actions: ActionList) =
            drawPartyManagerScreen(partyName, { y -> drawSingleHero(y, heroes, heroIndex, heroType, currentText) }, { y ->
                var currentY = y
                //Fill blank space
                (currentY until currentY + 2).forEach { drawBlankLine(it) }
                currentY += 2

                //Title
                val padding = 10
                ansi.cursor(currentY, 1)
                ansi.a('│')
                (0 until padding).forEach { ansi.a(' ') }
                val chooseName = loadString("new_hero_name")
                ansi.a("  ").a(chooseName)
                (2 + padding + 2 + chooseName.length until CONSOLE_WIDTH).forEach { ansi.a(' ') }
                ansi.a('│')
                currentY++

                //Top frame
                ansi.cursor(currentY, 1)
                ansi.a('│')
                (0 until padding).forEach { ansi.a(' ') }
                ansi.a('╔')
                (2 + padding + 1 until CONSOLE_WIDTH - padding - 1).forEach { ansi.a('═') }
                ansi.a('╗')
                (CONSOLE_WIDTH - padding until CONSOLE_WIDTH).forEach { ansi.a(' ') }
                ansi.a('│')
                currentY++

                //Current input
                val center = (CONSOLE_WIDTH - currentText.length - 1) / 2
                ansi.cursor(currentY, 1)
                ansi.a('│')
                (0 until padding).forEach { ansi.a(' ') }
                ansi.a('║')
                (2 + padding + 1 until center).forEach { ansi.a(' ') }
                ansi.color(Color.LIGHT_YELLOW).a(currentText.toUpperCase()).a('_').reset()
                (center + currentText.length + 1 until CONSOLE_WIDTH - padding - 1).forEach { ansi.a(' ') }
                ansi.a('║')
                (CONSOLE_WIDTH - padding until CONSOLE_WIDTH).forEach { ansi.a(' ') }
                ansi.a('│')
                currentY++

                //Bottom frame
                ansi.cursor(currentY, 1)
                ansi.a('│')
                (0 until padding).forEach { ansi.a(' ') }
                ansi.a('╚')
                (2 + padding + 1 until CONSOLE_WIDTH - padding - 1).forEach { ansi.a('═') }
                ansi.a('╝')
                (CONSOLE_WIDTH - padding until CONSOLE_WIDTH).forEach { ansi.a(' ') }
                ansi.a('│')
                currentY++

                currentY - y
            }, statusMessage, actions)

    //------------------------------------------------------------------------------------------------------------------

    override fun drawPartyNameEditScreen(currentText: String, statusMessage: StatusMessage, actions: ActionList) {
        val offsetY = CONSOLE_HEIGHT / 2

        //Blank lines
        (1..offsetY - 2).forEach { drawBlankLine(it, false) }

        //Status message
        drawStatusMessage(offsetY - 3, statusMessage, false)
        drawBlankLine(offsetY - 2, false)

        //Top border
        ansi.cursor(offsetY - 1, 1)
        ansi.a('┌')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┐')

        //Current input
        drawCenteredCaption(offsetY, currentText.toUpperCase() + "_", Color.LIGHT_YELLOW)

        //Bottom border
        ansi.cursor(offsetY + 1, 1)
        ansi.a('└')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┘')

        //Blank lines
        (offsetY + 2 until CONSOLE_HEIGHT).forEach { drawBlankLine(it, false) }

        //Action list
        drawActionList(CONSOLE_HEIGHT, actions, false)

        //Finalize
        render()
    }

}