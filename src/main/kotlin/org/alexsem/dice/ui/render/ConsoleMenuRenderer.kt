package org.alexsem.dice.ui.render

import org.alexsem.dice.model.Hero
import org.alexsem.dice.ui.ActionList
import org.alexsem.dice.ui.StatusMessage
import org.alexsem.dice.ui.render.strings.StringLoader
import org.alexsem.dice.ui.storage.PartyInfoShort
import org.alexsem.dice.model.LocalizedString
import org.fusesource.jansi.Ansi
import kotlin.math.max
import kotlin.math.min

class ConsoleMenuRenderer(loader: StringLoader) : ConsoleRenderer(loader), MenuRenderer {

    override val partiesPerPage = 3

    override val scenariosPerPage = CONSOLE_HEIGHT - 10

    /**
     * Draw single stored party info
     * @param offsetY Vertical position
     * @param offsetX Horizontal position
     * @param width Image width
     * @param party Party to draw
     * @param index Party index (may be null)
     * @param shadowed true to draw party dimmed, false for normal colors
     */
    private fun drawSingleParty(offsetY: Int, offsetX: Int, width: Int, party: PartyInfoShort, index: Int? = null, shadowed: Boolean = false) {
        val maxTextWidth = width - 4
        fun String.trimToSize() = this.substring(0, min(this.length, maxTextWidth))
        if (shadowed) {
            ansi.color(Color.DARK_GRAY)
        }

        //Top border
        ansi.cursor(offsetY, offsetX)
        ansi.a('╔')
        (2 until width).forEach { ansi.a('═') }
        ansi.a('╗')

        //Party name
        val title = party.partyName.trimToSize().toUpperCase()
        val titleOffset = if (title.length < maxTextWidth) (maxTextWidth - title.length) / 2 else 0
        ansi.cursor(offsetY + 1, offsetX)
        ansi.a("║ ")
        (0 until titleOffset).forEach { ansi.a(' ') }
        if (!shadowed) {
            ansi.color(Color.LIGHT_YELLOW).a(title).reset()
        } else {
            ansi.a(title)
        }
        (titleOffset + title.length until maxTextWidth).forEach { ansi.a(' ') }
        ansi.a(" ║")

        //Separator
        ansi.cursor(offsetY + 2, offsetX)
        ansi.a('╟')
        (2 until width).forEach { ansi.a('─') }
        ansi.a('╢')

        //Hero names
        val heroCount = min(party.heroNames.size, party.heroTypes.size)
        (0 until heroCount).forEach { h ->
            val name = party.heroNames[h].trimToSize().toUpperCase()
            ansi.cursor(offsetY + 3 + h, offsetX)
            ansi.a("║ ")
            if (!shadowed) {
                ansi.color(heroColors[party.heroTypes[h]]).a(name).reset()
            } else {
                ansi.a(name)
            }
            (name.length until maxTextWidth).forEach { ansi.a(' ') }
            ansi.a(" ║")
        }

        //Blank lines
        (heroCount until 5).forEach { h ->
            ansi.cursor(offsetY + 3 + h, offsetX)
            ansi.a('║')
            (2 until width).forEach { ansi.a(' ') }
            ansi.a('║')
        }

        //Separator
        ansi.cursor(offsetY + 8, offsetX)
        ansi.a('╟')
        (2 until width).forEach { ansi.a('─') }
        ansi.a('╢')

        //Adventure name
        val adventure = party.adventureName[language].trimToSize()
        ansi.cursor(offsetY + 9, offsetX)
        ansi.a("║ ")
        ansi.a(adventure)
        (adventure.length until maxTextWidth).forEach { ansi.a(' ') }
        ansi.a(" ║")

        //Bottom border
        ansi.cursor(offsetY + 10, offsetX)
        ansi.a('╚')
        (2 until width).forEach { ansi.a('═') }
        ansi.a('╝')

        //Index
        ansi.cursor(offsetY + 11, offsetX)
        if (index != null && !shadowed) {
            val indexOffset = (width - 3) / 2
            (0 until indexOffset).forEach { ansi.a(' ') }
            ansi.color(Color.LIGHT_YELLOW)
            ansi.a("(${shortcut(index)})").reset()
            (indexOffset + 3 until width).forEach { ansi.a(' ') }
        } else {
            (0 until width).forEach { ansi.a(' ') }
        }

        ansi.reset()
    }

    override fun drawPartyList(parties: List<PartyInfoShort>, pickedIndex: Int?, page: Int, totalPages: Int, statusMessage: StatusMessage, actions: ActionList) {
        var currentY = 2

        //Top border
        ansi.cursor(1, 1)
        ansi.a('┌')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┐')

        //Page title
        ansi.cursor(currentY, 1)
        val title = loadString("choose_party").toUpperCase()
        val pagesText = if (totalPages > 1) " ($page/$totalPages)" else ""
        ansi.a("│ ")
        ansi.a(title.toUpperCase()).a(pagesText).a(':')
        (3 + title.length + pagesText.length + 1 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')
        currentY++

        //Blank lines
        (1..3).forEach { drawBlankLine(currentY++) }

        //Parties
        if (parties.isNotEmpty()) {
            val width = (CONSOLE_WIDTH - 8) / partiesPerPage - 4
            //Left border
            (0 until 12).forEach { y ->
                ansi.cursor(currentY + y, 1)
                ansi.a("│   ")
            }
            //Parties
            var position = 0
            var currentX = 5
            ((page - 1) * partiesPerPage until min(page * partiesPerPage, parties.size)).forEach { index ->
                //Left offset
                (0 until 12).forEach { y ->
                    ansi.cursor(currentY + y, currentX)
                    ansi.a("  ")
                }
                currentX += 2
                //Party
                if (pickedIndex != null) {
                    drawSingleParty(currentY, currentX, width, parties[index], null, pickedIndex != index)
                } else {
                    drawSingleParty(currentY, currentX, width, parties[index], position)
                }
                currentX += width
                //Right offset
                (0 until 12).forEach { y ->
                    ansi.cursor(currentY + y, currentX)
                    ansi.a("  ")
                }
                currentX += 2
                position++
            }
            //Right border
            (0 until 12).forEach { y ->
                ansi.cursor(currentY + y, currentX)
                (currentX until CONSOLE_WIDTH).forEach { ansi.a(' ') }
                ansi.a('│')
            }
            currentY += 12
        } else {
            (1..5).forEach { drawBlankLine(currentY++) }
            drawCenteredCaption(currentY++, loadString("empty").toUpperCase(), Color.DARK_GRAY)
            (1..6).forEach { drawBlankLine(currentY++) }
        }

        //Blank lines
        val separatorY = CONSOLE_HEIGHT - 2 - calculateActionListHeight(actions)
        (currentY until separatorY).forEach { drawBlankLine(it) }

        //Separator
        ansi.cursor(separatorY, 1)
        ansi.a('├')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┤')

        //Status and actions
        drawStatusMessage(separatorY + 1, statusMessage)
        drawActionList(separatorY + 2, actions)

        //Bottom border
        ansi.cursor(CONSOLE_HEIGHT, 1)
        ansi.a('└')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┘')

        //Finalize
        render()
    }

    //------------------------------------------------------------------------------------------------------------------

    override fun drawAdventureList(names: List<LocalizedString>, descriptions: List<LocalizedString>, selectedIndex: Int, statusMessage: StatusMessage, actions: ActionList) {
        var currentY = 1

        //Blank lines
        drawBlankLine(currentY++, false)

        //Page title
        ansi.cursor(currentY++, 1)
        val title = loadString("choose_adventure").toUpperCase()
        val pagesText = if (names.size > 1) " (${selectedIndex + 1}/${names.size})" else ""
        ansi.a("  ")
        ansi.a(title.toUpperCase()).a(pagesText).a(':')
        (3 + title.length + pagesText.length + 1 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a(' ')

        //Adventures
        if (names.isNotEmpty()) {
            //Calculations
            val name = names[selectedIndex][language].toUpperCase()
            val description = descriptions[selectedIndex][language]
            val contentHeight = 4 + calculateMultilineTextHeight(description)
            val offsetY = (CONSOLE_HEIGHT - contentHeight) / 2 + 1
            //Blank lines
            (currentY until offsetY).forEach { drawBlankLine(it, false) }
            currentY = offsetY
            //Top border
            ansi.cursor(currentY++, 1)
            ansi.a('┌')
            (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
            ansi.a('┐')
            //Name
            val nameLength = name.length + 6
            val centerX = (CONSOLE_WIDTH - nameLength) / 2
            val color = if (names.size > 1) Color.LIGHT_YELLOW else Color.DARK_GRAY
            ansi.cursor(currentY++, 1).a('│')
            (2 until centerX).forEach { ansi.a(' ') }
            ansi.color(color).a("<  ").reset()
            ansi.color(Color.LIGHT_YELLOW).a(name).reset()
            ansi.color(color).a("  >").reset()
            (nameLength + centerX until CONSOLE_WIDTH).forEach { ansi.a(' ') }
            ansi.a('│')
            //Separator
            ansi.cursor(currentY++, 1)
            ansi.a('├')
            (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
            ansi.a('┤')
            //Description
            currentY += drawMultilineText(currentY, description)
            //Bottom border
            ansi.cursor(currentY++, 1)
            ansi.a('└')
            (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
            ansi.a('┘')
        } else {
            val centerY = CONSOLE_HEIGHT / 2
            (currentY until centerY).forEach { drawBlankLine(it, false) }
            currentY = centerY
            drawCenteredCaption(currentY++, loadString("empty").toUpperCase(), Color.DARK_GRAY, false)
        }

        //Blank lines
        val statusTop = CONSOLE_HEIGHT - calculateActionListHeight(actions, false)
        (currentY until statusTop).forEach { drawBlankLine(it, false) }

        //Status and actions
        drawStatusMessage(statusTop, statusMessage, false)
        drawActionList(statusTop + 1, actions, false)

        //Finalize
        render()
    }

    //------------------------------------------------------------------------------------------------------------------
    override fun drawScenarioList(adventureName: LocalizedString, phaseNames: List<LocalizedString>, progress: Int, heroes: List<Hero>, pickedIndex: Int?, page: Int, totalPages: Int, statusMessage: StatusMessage, actions: ActionList) {
        var currentY = 1
        val actionListHeight = calculateActionListHeight(actions)
        val offsetY = (CONSOLE_HEIGHT - actionListHeight - 6 - scenariosPerPage) / 2 + 5
        val heroesTitle = loadString("heroes") + ":"
        val heroNameLength = max(heroesTitle.length, heroes.map { it.name.length }.max() ?: 0)
        val separatorX = CONSOLE_WIDTH - heroNameLength - 3

        //Top border
        ansi.cursor(currentY++, 1)
        ansi.a('┌')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┐')
        //Adventure name
        drawCenteredCaption(currentY++, adventureName[language].toUpperCase(), Color.LIGHT_YELLOW)
        //Separator
        ansi.cursor(currentY++, 1)
        ansi.a('├')
        (2 until separatorX).forEach { ansi.a('─') }
        ansi.a('┬')
        (separatorX + 1 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┤')

        //Page title
        ansi.cursor(currentY++, 1)
        val title = loadString("choose_scenario").toUpperCase()
        val pagesText = if (totalPages > 1) " ($page/$totalPages)" else ""
        ansi.a("│ ")
        ansi.a(title.toUpperCase()).a(pagesText).a(':')
        (3 + title.length + pagesText.length + 1 until separatorX).forEach { ansi.a(' ') }
        ansi.a("│ ").a(heroesTitle)
        (separatorX + 2 + heroesTitle.length until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')

        //First blank line
        ansi.cursor(currentY++, 1)
        ansi.a('│')
        (2 until separatorX).forEach { ansi.a(' ') }
        ansi.a('├')
        (separatorX + 1 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┤')

        //Blank lines
        (currentY until offsetY).forEach { drawBlankLine(it) }
        currentY = offsetY

        //Scenario list
        if (phaseNames.isNotEmpty()) {
            var h = 0
            ((page - 1) * scenariosPerPage until min(page * scenariosPerPage, phaseNames.size)).forEach { index ->
                ansi.cursor(currentY, 1)
                val name = phaseNames[index][language].toUpperCase()
                ansi.a("│      ")
                if (index <= progress) {
                    ansi.color(Color.LIGHT_YELLOW)
                    when {
                        pickedIndex != null -> ansi.a("    ")
                        index == progress -> ansi.a("──> ")
                        else -> ansi.a("(${shortcut(h)}) ").reset()
                    }
                    if (pickedIndex ?: index != index) {
                        ansi.color(Color.DARK_GRAY)
                    }
                    ansi.a(name).reset()
                    (8 + 4 + name.length until CONSOLE_WIDTH).forEach { ansi.a(' ') }
                } else {
                    ansi.a("    ")
                    ansi.color(Color.DARK_GRAY)
                    ansi.a("???").reset()
                    (8 + 4 + 3 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
                }
                ansi.a('│')
                h++
                currentY++
            }
        } else {
            val centerY = CONSOLE_HEIGHT / 2
            (currentY until centerY).forEach { drawBlankLine(it) }
            currentY = centerY
            drawCenteredCaption(currentY++, loadString("empty").toUpperCase(), Color.DARK_GRAY)
        }

        //Blank lines
        val separatorY = CONSOLE_HEIGHT - 2 - actionListHeight
        (currentY until separatorY).forEach { drawBlankLine(it) }

        //Heroes
        heroes.forEachIndexed { index, hero ->
            ansi.cursor(6 + index, CONSOLE_WIDTH - 1 - heroNameLength - 2)
            ansi.a("│ ")
            ansi.color(heroColors[hero.type])
            ansi.a(hero.name.toUpperCase()).reset()
            (hero.name.length until heroNameLength).forEach { ansi.a(' ') }
            ansi.a(" │")
        }
        ansi.cursor(6 + heroes.size, CONSOLE_WIDTH - 1 - heroNameLength - 2)
        ansi.a("└─")
        (0 until heroNameLength).forEach { ansi.a('─') }
        ansi.a("─┤")

        //Separator
        ansi.cursor(separatorY, 1)
        ansi.a('├')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┤')

        //Status and actions
        drawStatusMessage(separatorY + 1, statusMessage)
        drawActionList(separatorY + 2, actions)

        //Bottom border
        ansi.cursor(CONSOLE_HEIGHT, 1)
        ansi.a('└')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┘')

        //Finalize
        render()
    }

    //------------------------------------------------------------------------------------------------------------------
    override fun drawMainMenu(actions: ActionList) {
        var currentY = 1
        val titleColor = Color.DARK_YELLOW
        val subtitleColor = Color.LIGHT_GRAY

        //Blank lines
        drawBlankLine(currentY++, false)

        //Title
        drawCenteredCaption(currentY++, "█████████     ████████     ████████    █████████", titleColor, false)
        drawCenteredCaption(currentY++, "██      ██       ██       ██      ██   ██       ", titleColor, false)
        drawCenteredCaption(currentY++, "██      ██       ██       ██           ██       ", titleColor, false)
        drawCenteredCaption(currentY++, "██      ██       ██       ██           ███████  ", titleColor, false)
        drawCenteredCaption(currentY++, "██      ██       ██       ██           ██       ", titleColor, false)
        drawCenteredCaption(currentY++, "██      ██       ██       ██      ██   ██       ", titleColor, false)
        drawCenteredCaption(currentY++, "█████████     ████████     ████████    █████████", titleColor, false)

        //Blank lines
        drawBlankLine(currentY++, false)

        //Subtitle
        drawCenteredCaption(currentY++, " @@@@  @@@@@   @@@    @@@@    @@@   @@@@    @@@@", subtitleColor, false)
        drawCenteredCaption(currentY++, "@        @    @   @   @   @    @    @      @    ", subtitleColor, false)
        drawCenteredCaption(currentY++, " @@@     @    @   @   @@@@     @    @@@     @@@ ", subtitleColor, false)
        drawCenteredCaption(currentY++, "    @    @    @   @   @  @     @    @          @", subtitleColor, false)
        drawCenteredCaption(currentY++, "@@@@     @     @@@    @   @   @@@   @@@@   @@@@ ", subtitleColor, false)

        //Blank lines
        drawBlankLine(currentY++, false)

        //Menu
        val contentHeight = actions.size * 2
        val contentWidth = (0 until actions.size).asSequence()
                .map { actions[it] }
                .map { a -> stringHelper.loadActionData(a).sumBy { it.length } + 20 }
                .max() ?: 0
        val offsetX = (CONSOLE_WIDTH - contentWidth) / 2
        val offsetY = (CONSOLE_HEIGHT - contentHeight + currentY) / 2

        //Fill blank space
        (currentY until offsetY).forEach { drawBlankLine(it, false) }

        //Top frame
        ansi.cursor(offsetY, offsetX)
        ansi.eraseLine(Ansi.Erase.BACKWARD)
        ansi.a('╔')
        (0 until contentWidth).forEach { ansi.a('═') }
        ansi.a('╗')
        ansi.eraseLine(Ansi.Erase.FORWARD)

        //Actions
        actions.forEachIndexed { index, action ->
            val data = stringHelper.loadActionData(action)
            val name = if (data[1][0].isLowerCase()) data[0] + data[1] else data[1]
            ansi.cursor(offsetY + index * 2 + 1, offsetX)
            ansi.eraseLine(Ansi.Erase.BACKWARD)
            ansi.a('║')
            ansi.color(if (action.isEnabled) Color.LIGHT_YELLOW else Color.DARK_GRAY)
            ansi.a(" (${data[0]}) ")
            if (action.isEnabled) {
                ansi.reset()
            }
            ansi.a(name.toUpperCase()).reset()
            (offsetX + 3 + data[0].length + 1 + name.length until offsetX + contentWidth).forEach { ansi.a(' ') }
            ansi.a('║')
            ansi.eraseLine(Ansi.Erase.FORWARD)
            //Separator
            if (index < actions.size - 1) {
                ansi.cursor(offsetY + index * 2 + 2, offsetX)
                ansi.eraseLine(Ansi.Erase.BACKWARD)
                ansi.a('╟')
                (0 until contentWidth).forEach { ansi.a('─') }
                ansi.a('╢')
                ansi.eraseLine(Ansi.Erase.FORWARD)
            }
        }

        //Bottom frame
        ansi.cursor(offsetY + contentHeight, offsetX)
        ansi.eraseLine(Ansi.Erase.BACKWARD)
        ansi.a('╚')
        (0 until contentWidth).forEach { ansi.a('═') }
        ansi.a('╝')
        ansi.eraseLine(Ansi.Erase.FORWARD)

        //Fill blank space
        (offsetY + contentHeight + 1..CONSOLE_HEIGHT).forEach { drawBlankLine(it, false) }

        //Finalize
        render()
    }

}