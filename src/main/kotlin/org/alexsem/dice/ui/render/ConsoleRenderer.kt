package org.alexsem.dice.ui.render

import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Hero
import org.alexsem.dice.ui.ActionList
import org.alexsem.dice.ui.StatusMessage
import org.alexsem.dice.ui.render.strings.StringLoadHelper
import org.alexsem.dice.ui.render.strings.StringLoader
import org.fusesource.jansi.Ansi
import org.fusesource.jansi.AnsiConsole
import java.util.*

const val CONSOLE_WIDTH = 80
const val CONSOLE_HEIGHT = 24

/**
 * Abstract superclass for all console renderers
 */
abstract class ConsoleRenderer(private val strings: StringLoader) : Renderer {

    protected lateinit var ansi: Ansi
    protected val stringHelper = StringLoadHelper(strings)
    protected val language: String
        get() = Locale.getDefault().language

    private val patternColors = mapOf(
            strings.loadString("physical").toUpperCase().toRegex() to Color.LIGHT_BLUE,
            strings.loadString("somatic").toUpperCase().toRegex() to Color.LIGHT_GREEN,
            strings.loadString("mental").toUpperCase().toRegex() to Color.LIGHT_MAGENTA,
            strings.loadString("verbal").toUpperCase().toRegex() to Color.LIGHT_YELLOW,
            strings.loadString("divine").toUpperCase().toRegex() to Color.LIGHT_CYAN,
            strings.loadString("ally").toUpperCase().toRegex() to Color.WHITE,
            strings.loadString("wound").toUpperCase().toRegex() to Color.DARK_GRAY,
            strings.loadString("enemy").toUpperCase().toRegex() to Color.DARK_RED,
            strings.loadString("villain").toUpperCase().toRegex() to Color.LIGHT_RED,
            strings.loadString("obstacle").toUpperCase().toRegex() to Color.DARK_YELLOW,
            strings.loadString("end_of_turn").toUpperCase().toRegex() to Color.WHITE,
            strings.loadString("location_closure").toUpperCase().toRegex() to Color.WHITE,
            strings.loadString("paralysis").toUpperCase().toRegex() to Color.WHITE,
            strings.loadString("concussion").toUpperCase().toRegex() to Color.WHITE,
            strings.loadString("dementia").toUpperCase().toRegex() to Color.WHITE,
            strings.loadString("muteness").toUpperCase().toRegex() to Color.WHITE,
            strings.loadString("curse").toUpperCase().toRegex() to Color.WHITE,
            strings.loadString("betrayal").toUpperCase().toRegex() to Color.WHITE,
            (strings.loadString("max") + "\\.").toRegex() to Color.LIGHT_YELLOW,
            (strings.loadString("min") + "\\.").toRegex() to Color.LIGHT_YELLOW,
            (strings.loadString("sum") + "\\.").toRegex() to Color.LIGHT_YELLOW,
            (strings.loadString("avg") + "\\.").toRegex() to Color.LIGHT_YELLOW,
            "dX".toRegex() to Color.LIGHT_YELLOW,
            "[( ](d4|d6|d8|d10|d12)[. )]".toRegex() to Color.LIGHT_YELLOW,
            "[( ](\\d+)[. )]".toRegex() to Color.LIGHT_YELLOW,
            "\\+\\d+".toRegex() to Color.LIGHT_GREEN,
            "-\\d+".toRegex() to Color.LIGHT_RED
    )

    protected val dieColors = mapOf(
            Die.Type.PHYSICAL to Color.LIGHT_BLUE,
            Die.Type.SOMATIC to Color.LIGHT_GREEN,
            Die.Type.MENTAL to Color.LIGHT_MAGENTA,
            Die.Type.VERBAL to Color.LIGHT_YELLOW,
            Die.Type.DIVINE to Color.LIGHT_CYAN,
            Die.Type.WOUND to Color.DARK_GRAY,
            Die.Type.ENEMY to Color.DARK_RED,
            Die.Type.VILLAIN to Color.LIGHT_RED,
            Die.Type.OBSTACLE to Color.DARK_YELLOW,
            Die.Type.ALLY to Color.WHITE
    )

    protected val heroColors = mapOf(
            Hero.Type.BRAWLER to Color.LIGHT_BLUE,
            Hero.Type.HUNTER to Color.LIGHT_GREEN,
            Hero.Type.MYSTIC to Color.LIGHT_MAGENTA,
            Hero.Type.PROPHET to Color.LIGHT_CYAN,
            Hero.Type.MENTOR to Color.LIGHT_YELLOW
    )

    /**
     * Get visual representation of the numeric index
     * @param index Number to convert
     * @return Character shortcut
     */
    protected open fun shortcut(index: Int) = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ"[index]

    init {
        AnsiConsole.systemInstall()
        clearScreen()
        resetAnsi()
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Load localized string for specific key
     * @param key String key
     * @return Corresponding string
     */
    protected fun loadString(key: String) = strings.loadString(key)

    /**
     * Generates new Ansi object, removing everything created before
     */
    private fun resetAnsi() {
        ansi = Ansi.ansi()
    }

    /**
     * Clear console
     */
    final override fun clearScreen() {
        print(Ansi.ansi().cursor(1, 1).eraseScreen(Ansi.Erase.FORWARD))
    }

    /**
     * Draws previously prepared render
     */
    protected fun render() {
        ansi.cursor(CONSOLE_HEIGHT, CONSOLE_WIDTH)
        print(ansi.toString())
        resetAnsi()
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Draw big 5x5 numbers
     * @param offsetX Horizontal position
     * @param offsetY Vertical position
     * @param number Number to draw
     */
    protected fun drawBigNumber(offsetX: Int, offsetY: Int, number: Int): Unit = with(ansi) {
        var currentX = offsetX
        cursor(offsetY, currentX)
        val text = number.toString()
        text.forEach {
            when (it) {
                '0' -> {
                    cursor(offsetY, currentX)
                    a(" ███  ")
                    cursor(offsetY + 1, currentX)
                    a("█   █ ")
                    cursor(offsetY + 2, currentX)
                    a("█   █ ")
                    cursor(offsetY + 3, currentX)
                    a("█   █ ")
                    cursor(offsetY + 4, currentX)
                    a(" ███  ")
                }
                '1' -> {
                    cursor(offsetY, currentX)
                    a("  █   ")
                    cursor(offsetY + 1, currentX)
                    a(" ██   ")
                    cursor(offsetY + 2, currentX)
                    a("█ █   ")
                    cursor(offsetY + 3, currentX)
                    a("  █   ")
                    cursor(offsetY + 4, currentX)
                    a("█████ ")
                }
                '2' -> {
                    cursor(offsetY, currentX)
                    a(" ███  ")
                    cursor(offsetY + 1, currentX)
                    a("█   █ ")
                    cursor(offsetY + 2, currentX)
                    a("   █  ")
                    cursor(offsetY + 3, currentX)
                    a("  █   ")
                    cursor(offsetY + 4, currentX)
                    a("█████ ")
                }
                '3' -> {
                    cursor(offsetY, currentX)
                    a("████  ")
                    cursor(offsetY + 1, currentX)
                    a("    █ ")
                    cursor(offsetY + 2, currentX)
                    a("  ██  ")
                    cursor(offsetY + 3, currentX)
                    a("    █ ")
                    cursor(offsetY + 4, currentX)
                    a("████  ")
                }
                '4' -> {
                    cursor(offsetY, currentX)
                    a("   █  ")
                    cursor(offsetY + 1, currentX)
                    a("  ██  ")
                    cursor(offsetY + 2, currentX)
                    a(" █ █  ")
                    cursor(offsetY + 3, currentX)
                    a("█████ ")
                    cursor(offsetY + 4, currentX)
                    a("   █  ")
                }
                '5' -> {
                    cursor(offsetY, currentX)
                    a("█████ ")
                    cursor(offsetY + 1, currentX)
                    a("█     ")
                    cursor(offsetY + 2, currentX)
                    a("████  ")
                    cursor(offsetY + 3, currentX)
                    a("    █ ")
                    cursor(offsetY + 4, currentX)
                    a("████  ")
                }
                '6' -> {
                    cursor(offsetY, currentX)
                    a(" ███  ")
                    cursor(offsetY + 1, currentX)
                    a("█     ")
                    cursor(offsetY + 2, currentX)
                    a("████  ")
                    cursor(offsetY + 3, currentX)
                    a("█   █ ")
                    cursor(offsetY + 4, currentX)
                    a(" ███  ")
                }
                '7' -> {
                    cursor(offsetY, currentX)
                    a("█████ ")
                    cursor(offsetY + 1, currentX)
                    a("   █  ")
                    cursor(offsetY + 2, currentX)
                    a("  █   ")
                    cursor(offsetY + 3, currentX)
                    a("  █   ")
                    cursor(offsetY + 4, currentX)
                    a("  █   ")
                }
                '8' -> {
                    cursor(offsetY, currentX)
                    a(" ███  ")
                    cursor(offsetY + 1, currentX)
                    a("█   █ ")
                    cursor(offsetY + 2, currentX)
                    a(" ███  ")
                    cursor(offsetY + 3, currentX)
                    a("█   █ ")
                    cursor(offsetY + 4, currentX)
                    a(" ███  ")
                }
                '9' -> {
                    cursor(offsetY, currentX)
                    a(" ███  ")
                    cursor(offsetY + 1, currentX)
                    a("█   █ ")
                    cursor(offsetY + 2, currentX)
                    a(" ████ ")
                    cursor(offsetY + 3, currentX)
                    a("    █ ")
                    cursor(offsetY + 4, currentX)
                    a(" ███  ")
                }
            }
            currentX += 6
        }
    }


    /**
     * Draws horizontal line across all screen
     * @param offsetY Vertical position
     * @param filler Character to fill line with
     */
    protected fun drawHorizontalLine(offsetY: Int, filler: Char) {
        ansi.cursor(offsetY, 1)
        (1..CONSOLE_WIDTH).forEach { ansi.a(filler) }
    }

    /**
     * Draw full-width line of spaces
     * @param offsetY Vertical position
     * @param drawBorders true to draw vertical borders to the left and right
     */
    protected fun drawBlankLine(offsetY: Int, drawBorders: Boolean = true) {
        ansi.cursor(offsetY, 1)
        if (drawBorders) {
            ansi.a('│')
            (2 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
            ansi.a('│')
        } else {
            ansi.eraseLine(Ansi.Erase.ALL)
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Draws some colored text centered horizontally
     * @param offsetY Vertical position
     * @param text Text to render
     * @param color Text color
     * @param drawBorders true to draw vertical borders to the left and right of text
     */
    protected fun drawCenteredCaption(offsetY: Int, text: String, color: Color, drawBorders: Boolean = true) {
        val center = (CONSOLE_WIDTH - text.length) / 2
        ansi.cursor(offsetY, 1)
        ansi.a(if (drawBorders) '│' else ' ')
        (2 until center).forEach { ansi.a(' ') }
        ansi.color(color).a(text).reset()
        (text.length + center until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a(if (drawBorders) '│' else ' ')
    }

    /**
     * Draws some text centered horizontally
     * @param offsetY Vertical position
     * @param text Text to render
     * @param drawBorders true to draw vertical borders to the left and right of text
     * @return number of lines needed
     */
    protected fun drawMultilineCenteredText(offsetY: Int, text: String, drawBorders: Boolean = true): Int {
        val width = CONSOLE_WIDTH - if (drawBorders) 4 else 2
        var x = 0
        var y = 1
        val lines = text.split(' ').asSequence()
                .groupBy {
                    if (x + it.length >= width) {
                        y++
                        x = 0
                    }
                    x += it.length + 1
                    y
                }
                .values
                .map { it.joinToString(" ").trim() }
                .toList()
        lines.forEachIndexed { i, line ->
            val center = (CONSOLE_WIDTH - line.length) / 2
            ansi.cursor(offsetY + i, 1)
            ansi.a(if (drawBorders) '│' else ' ')
            (2 until center).forEach { ansi.a(' ') }
            ansi.a(line)
            (line.length + center until CONSOLE_WIDTH).forEach { ansi.a(' ') }
            ansi.a(if (drawBorders) '│' else ' ')
        }
        return lines.size
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Calculates number of lines the multiline text will take
     * @param text Text to calculate
     * @param drawBorders true to draw vertical borders to the left and right of text
     * @return Number of lines this text requires to fit
     */
    protected fun calculateMultilineTextHeight(text: String, drawBorders: Boolean = true): Int {
        //Line break support
        if ('\n' in text) {
            return text.split('\n').map { calculateMultilineTextHeight(it, drawBorders) }.sum()
        }
        //Single-line text
        var currentX = if (drawBorders) 2 else 1
        var lineCount = 1
        val scanner = Scanner(text)
        while (scanner.hasNext()) {
            val token = scanner.next()
            if (currentX + token.length > CONSOLE_WIDTH - (if (drawBorders) 1 else 0)) { //Next line
                lineCount++
                currentX = if (drawBorders) 2 else 1
            }
            currentX += token.length + 1
        }
        return lineCount
    }


    /**
     * Draws some long text on multiple lines, using all substitutions which are needed
     * @param offsetY Vertical position
     * @param text Text to render
     * @param drawBorders true to draw vertical borders to the left and right of text
     * @param startLine Line (inclusive, 1-based) starting from which render should begin (-1 to start from the very first line)
     * @param endLine Line (inclusive, 1-based) on which the render should end (-1 to end on the very last line)
     * @param additionalSpans Additional color spans to create (optional)
     * @return Number of lines this text required to fit
     */
    @SafeVarargs
    protected fun drawMultilineText(offsetY: Int, text: String, drawBorders: Boolean = true, startLine: Int = -1, endLine: Int = -1, vararg additionalSpans: Pair<Regex, Color>): Int {
        //Setup
        var currentX = 1
        var currentY = offsetY
        val rightBorder = CONSOLE_WIDTH - if (drawBorders) 1 else 0
        var currentIndex = 0

        //Prepare Span list
        val spans = (patternColors.asSequence().map { it.key to it.value } + additionalSpans.asSequence())
                .flatMap { e ->
                    (e.first.findAll(text)).map {
                        with(it.groups.last()!!) {
                            Span(this.range.start, this.range.endInclusive + 1, this.value, e.second)
                        }
                    }
                }.sorted().toList()

        //Preparation
        var nextSpanIndex = -1
        var isSpanStarted = false
        var nextSpan: Span
        var currentSubstring: String
        var currentColor: Color? = null
        var currentLine = 1

        //Check lines
        fun shouldDrawLine() = when {
            startLine > -1 && currentLine < startLine -> false
            endLine > -1 && currentLine > endLine -> false
            else -> true
        }

        fun <T : Any?> Ansi.a2(t: T) = when (shouldDrawLine()) {
            true -> a(t)
            false -> this
        }

        //Left border
        ansi.cursor(currentY, 1)
        if (drawBorders) {
            ansi.a2('│')
            currentX++
        }
        ansi.a2(' ')
        currentX++

        //Main loop
        while (currentIndex < text.length) {
            //Check spans
            if (spans.size > nextSpanIndex + 1) { //Span coming next
                nextSpan = spans[nextSpanIndex + 1]
                if (nextSpan.start > currentIndex) { //Span not yet started
                    currentSubstring = text.substring(currentIndex, nextSpan.start)
                    currentIndex = nextSpan.start
                } else { //Within span
                    isSpanStarted = true
                    nextSpanIndex++
                    ansi.color(nextSpan.color)
                    currentColor = nextSpan.color
                    currentSubstring = text.substring(currentIndex, nextSpan.end)
                    currentIndex = nextSpan.end
                }
            } else { //No more spans till the end of the string
                currentSubstring = text.substring(currentIndex)
                currentIndex = text.length
            }
            //Draw tokens
            val scanner = Scanner(currentSubstring).useDelimiter(" ")
            var needLeadingSpace = currentSubstring.startsWith(" ")
            while (scanner.hasNext()) {
                if (needLeadingSpace) {
                    ansi.a2(' ')
                    currentX++
                } else {
                    needLeadingSpace = true
                }
                fun newLine() {
                    (currentX..rightBorder).forEach { ansi.a2(' ') }
                    ansi.reset()
                    if (drawBorders) {
                        ansi.a2('│')
                    }
                    if (shouldDrawLine()) {
                        currentY++
                    }
                    currentLine++
                    currentX = 1
                    ansi.cursor(currentY, currentX)
                    if (drawBorders) {
                        ansi.a2('│')
                        currentX++
                    }
                    ansi.a2(' ')
                    currentX++
                    if (currentColor != null) {
                        ansi.color(currentColor)
                    }
                }

                fun drawToken(token: String) {
                    if (currentX + token.length > rightBorder) { //Next line
                        newLine()
                    }
                    ansi.a2(token)
                    currentX += token.length
                }
                with(scanner.next() as String) {
                    if ('\n' in this) {
                        for ((index, part) in this.split('\n').withIndex()) {
                            if (index > 0) {
                                newLine()
                            }
                            drawToken(part)
                        }
                    } else {
                        drawToken(this)
                    }
                }

            }
            //Adjustment
            if (currentSubstring.endsWith(" ")) {
                ansi.a2(' ')
                currentX++
            }
            //Check if reset is needed
            if (isSpanStarted) {
                ansi.reset()
                currentColor = null
                isSpanStarted = false
            }
        }

        //Right border
        (currentX..rightBorder).forEach { ansi.a2(' ') }
        if (drawBorders) {
            ansi.a2('│')
        }

        return currentY - offsetY + 1
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Draws current status message
     * @param offsetY Vertical position
     * @param message Text to draw
     * @param drawBorders true to draw vertical borders to the left and right of text
     */
    protected fun drawStatusMessage(offsetY: Int, message: StatusMessage, drawBorders: Boolean = true) {
        //Setup
        val messageText = stringHelper.loadStatusMessage(message)
        var currentX = 1
        val rightBorder = CONSOLE_WIDTH - if (drawBorders) 1 else 0
        var currentIndex = 0

        //Prepare span list
        val spans = patternColors.asSequence()
                .flatMap { e ->
                    (e.key.findAll(messageText)).map {
                        with(it.groups.last()!!) {
                            Span(this.range.start, this.range.endInclusive + 1, this.value, e.value)
                        }
                    }
                }.sorted().toList()

        //Left border
        ansi.cursor(offsetY, 1)
        if (drawBorders) {
            ansi.a('│')
            currentX++
        }
        ansi.a(' ')
        currentX++

        //Draw spans
        for (span in spans) {
            if (span.start > currentIndex) {
                ansi.a(messageText.substring(currentIndex, span.start))
            }
            ansi.color(span.color).a(span.data).reset()
            currentIndex = span.end
        }
        if (currentIndex < messageText.length) {
            ansi.a(messageText.substring(currentIndex))
        }
        currentX += messageText.length

        //Right border
        (currentX..rightBorder).forEach { ansi.a(' ') }
        if (drawBorders) {
            ansi.a('│')
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Calculate the height needed to draw specific list of actions
     * @param actions Actions to draw
     * @return number of lines the list takes
     * @param drawBorders true to draw vertical borders to the left and right of text
     */
    protected fun calculateActionListHeight(actions: ActionList, drawBorders: Boolean = true): Int {
        val leftBorder = if (drawBorders) 3 else 2
        val rightBorder = CONSOLE_WIDTH - if (drawBorders) 1 else 0
        var height = 1
        var currentX = leftBorder
        actions.forEach {
            val data = stringHelper.loadActionData(it)
            val length = data[0].length + 2 + data[1].length
            if (currentX + length >= rightBorder) {
                height++
                currentX = leftBorder
            }
            currentX += length + 2
        }
        return height
    }

    /**
     * Draw list of available actions
     * @param offsetY Vertical position
     * @param actions Actions to draw
     * @param drawBorders true to draw vertical borders to the left and right of text
     */
    protected fun drawActionList(offsetY: Int, actions: ActionList, drawBorders: Boolean = true) {
        val rightBorder = CONSOLE_WIDTH - if (drawBorders) 1 else 0
        var currentX = 1

        //Left border
        ansi.cursor(offsetY, 1)
        if (drawBorders) {
            ansi.a('│')
            currentX++
        }
        ansi.a(' ')
        currentX++

        //List of actions
        actions.forEach { action ->
            val data = stringHelper.loadActionData(action)
            val length = data[0].length + 2 + data[1].length
            if (currentX + length >= rightBorder) {
                (currentX..rightBorder).forEach { ansi.a(' ') }
                if (drawBorders) {
                    ansi.a('│')
                }
                ansi.cursor(offsetY + 1, 1)
                currentX = 1
                if (drawBorders) {
                    ansi.a('│')
                    currentX++
                }
                ansi.a(' ')
                currentX++
            }
            if (action.isEnabled) {
                ansi.color(Color.LIGHT_YELLOW)
            }
            ansi.a('(').a(data[0]).a(')').reset()
            ansi.a(data[1])
            ansi.a("  ")
            currentX += length + 2
        }

        //Right border
        (currentX..rightBorder).forEach { ansi.a(' ') }
        if (drawBorders) {
            ansi.a('│')
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Colors used by [Ansi]
     */
    protected enum class Color {
        BLACK, DARK_BLUE, DARK_GREEN, DARK_CYAN, DARK_RED, DARK_MAGENTA, DARK_YELLOW, LIGHT_GRAY,
        DARK_GRAY, LIGHT_BLUE, LIGHT_GREEN, LIGHT_CYAN, LIGHT_RED, LIGHT_MAGENTA, LIGHT_YELLOW, WHITE
    }

    /**
     * Set specified color
     * @param color Color to set
     */
    protected fun Ansi.color(color: Color?): Ansi = when (color) {
        Color.BLACK -> fgBlack()
        Color.DARK_BLUE -> fgBlue()
        Color.DARK_GREEN -> fgGreen()
        Color.DARK_CYAN -> fgCyan()
        Color.DARK_RED -> fgRed()
        Color.DARK_MAGENTA -> fgMagenta()
        Color.DARK_YELLOW -> fgYellow()
        Color.LIGHT_GRAY -> fg(Ansi.Color.WHITE)
        Color.DARK_GRAY -> fgBrightBlack()
        Color.LIGHT_BLUE -> fgBrightBlue()
        Color.LIGHT_GREEN -> fgBrightGreen()
        Color.LIGHT_CYAN -> fgBrightCyan()
        Color.LIGHT_RED -> fgBrightRed()
        Color.LIGHT_MAGENTA -> fgBrightMagenta()
        Color.LIGHT_YELLOW -> fgBrightYellow()
        Color.WHITE -> fgBright(Ansi.Color.WHITE)
        else -> this
    }

    /**
     * Set specified background color
     * @param color Color to set
     */
    protected fun Ansi.background(color: Color?): Ansi = when (color) {
        Color.BLACK -> ansi.bg(Ansi.Color.BLACK)
        Color.DARK_BLUE -> ansi.bg(Ansi.Color.BLUE)
        Color.DARK_GREEN -> ansi.bgGreen()
        Color.DARK_CYAN -> ansi.bg(Ansi.Color.CYAN)
        Color.DARK_RED -> ansi.bgRed()
        Color.DARK_MAGENTA -> ansi.bgMagenta()
        Color.DARK_YELLOW -> ansi.bgYellow()
        Color.LIGHT_GRAY -> ansi.bg(Ansi.Color.WHITE)
        Color.DARK_GRAY -> ansi.bgBright(Ansi.Color.BLACK)
        Color.LIGHT_BLUE -> ansi.bgBright(Ansi.Color.BLUE)
        Color.LIGHT_GREEN -> ansi.bgBrightGreen()
        Color.LIGHT_CYAN -> ansi.bgBright(Ansi.Color.CYAN)
        Color.LIGHT_RED -> ansi.bgBrightRed()
        Color.LIGHT_MAGENTA -> ansi.bgBright(Ansi.Color.MAGENTA)
        Color.LIGHT_YELLOW -> ansi.bgBrightYellow()
        Color.WHITE -> ansi.bgBright(Ansi.Color.WHITE)
        else -> this
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Special class used for text formatting
     */
    private inner class Span(val start: Int, val end: Int, val data: String, val color: Color) : Comparable<Span> {
        override fun compareTo(other: Span) = compareValuesBy(this, other, Span::start)
    }


}