package org.alexsem.dice.ui.render

import org.alexsem.dice.ui.Action
import org.alexsem.dice.ui.ActionList
import org.alexsem.dice.ui.StatusMessage
import org.alexsem.dice.ui.render.ConsoleRenderer.Color.*
import org.alexsem.dice.ui.render.strings.StringLoader
import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Hero
import org.alexsem.dice.model.Skill
import org.fusesource.jansi.Ansi

/**
 * Console implementation of the [UpgradeRenderer] interface
 */
class ConsoleUpgradeRenderer(loader: StringLoader) : ConsoleRenderer(loader), UpgradeRenderer {

    private val actionDieColors = mapOf(
            Action.Type.PHYSICAL to dieColors[Die.Type.PHYSICAL],
            Action.Type.SOMATIC to dieColors[Die.Type.SOMATIC],
            Action.Type.MENTAL to dieColors[Die.Type.MENTAL],
            Action.Type.VERBAL to dieColors[Die.Type.VERBAL],
            Action.Type.DIVINE to dieColors[Die.Type.DIVINE],
            Action.Type.ALLY to dieColors[Die.Type.ALLY]
    )
    private val actionDieTitles = mapOf(
            Action.Type.PHYSICAL to loadString("physical"),
            Action.Type.SOMATIC to loadString("somatic"),
            Action.Type.MENTAL to loadString("mental"),
            Action.Type.VERBAL to loadString("verbal"),
            Action.Type.DIVINE to loadString("divine"),
            Action.Type.ALLY to loadString("ally")
    )

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Draws top panel with hero name
     * @param hero Hero to draw
     */
    private fun drawHeroTopPanel(hero: Hero) {
        //Top border
        ansi.cursor(1, 1)
        ansi.a('┌')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┐')
        //Center row
        val name = hero.name.toUpperCase()
        if (hero.isAlive) {
            drawCenteredCaption(2, name, heroColors.getValue(hero.type))
        } else {
            val dead = loadString("dead").toUpperCase()
            val length = name.length + 2 + dead.length + 1
            val center = (CONSOLE_WIDTH - length) / 2
            ansi.cursor(2, 1).a('│')
            (2 until center).forEach { ansi.a(' ') }
            ansi.color(DARK_GRAY).a(name).reset()
            ansi.color(LIGHT_RED).a(" (").a(dead).a(')').reset()
            (length + center until CONSOLE_WIDTH).forEach { ansi.a(' ') }
            ansi.a('│')
        }
        //Bottom border
        ansi.cursor(3, 1)
        ansi.a('├')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┤')
    }

    //------------------------------------------------------------------------------------------------------------------
    override fun drawHeroDiceLimits(hero: Hero, plusOne: Die.Type?, statusMessage: StatusMessage, actions: ActionList) {
        drawHeroTopPanel(hero)
        var currentY = 4

        //Page title
        ansi.cursor(currentY, 1)
        val diceLimits = loadString("dice_limits")
        ansi.a("│ ")
        ansi.a(diceLimits.toUpperCase()).a(':')
        (3 + diceLimits.length + 1 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')
        currentY++

        //Blank lines
        (currentY..currentY + 1).forEach { drawBlankLine(it) }
        currentY += 2

        //Limits
        val limits = hero.getDiceLimits()
        val titles = limits.map { stringHelper.loadDieType(it.type).toUpperCase() }
        val maxTitleLength = (titles.map { it.length }.max() ?: 0) + 5
        val maxLimitLength = (limits.map { it.maximal }.max() ?: 0) * 4 + 4

        //Main loop
        for (index in titles.indices) {
            val limit = limits[index]
            val title = titles[index]
            var currentX = 1
            //Title
            ansi.a("│   ")
            currentX += 4
            ansi.color(dieColors[limit.type]).a(title).reset().a(':')
            (title.length + 1 until maxTitleLength).forEach { ansi.a(' ') }
            currentX += maxTitleLength
            //Limit
            ansi.background(DARK_CYAN).color(LIGHT_YELLOW)
            (1 until limit.current).forEach { ansi.a("    ") }
            ansi.a(String.format("%4d", limit.current)).reset()
            //Plus one
            var rangeStart = limit.current + 1
            if (plusOne != null && plusOne == limit.type) {
                ansi.background(DARK_CYAN).color(LIGHT_CYAN)
                ansi.a(String.format("%+4d", 1))
                ansi.reset()
                rangeStart++
            }
            //Reserve
            ansi.background(LIGHT_GRAY).color(BLACK)
            (rangeStart..limit.maximal).forEach { ansi.a(String.format("%+4d", it - limit.current)) }
            (limit.maximal * 4 until maxLimitLength).forEach { ansi.a(' ') }
            currentX += maxLimitLength
            ansi.reset()
            (currentX until CONSOLE_WIDTH).forEach { ansi.a(' ') }
            ansi.a('│')
            //Blank line
            currentY++
            drawBlankLine(currentY)
            currentY++
        }

        //Blank line
        drawBlankLine(currentY)
        currentY++

        //Favored type
        ansi.cursor(currentY, 1)
        ansi.a("│ ")
        val favoredDieType = loadString("favored_die_type")
        val type = stringHelper.loadDieType(hero.favoredDieType).toUpperCase()
        ansi.a(favoredDieType).a(' ')
        ansi.color(dieColors[hero.favoredDieType])
        ansi.a(type).reset()
        (3 + favoredDieType.length + 1 + type.length until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')
        currentY++

        //Blank lines
        val separatorY = CONSOLE_HEIGHT - 2 - calculateActionListHeight(actions)
        (currentY until separatorY).forEach { drawBlankLine(it) }

        //Separator
        ansi.cursor(separatorY, 1)
        ansi.a('├')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┤')

        //Status and actions
        drawStatusMessage(separatorY + 1, statusMessage)
        drawActionList(separatorY + 2, actions)

        //Bottom border
        ansi.cursor(CONSOLE_HEIGHT, 1)
        ansi.a('└')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┘')

        //Finalize
        render()
    }

    //------------------------------------------------------------------------------------------------------------------
    override fun drawHeroSkills(hero: Hero, skills: List<Skill>, statusMessage: StatusMessage, actions: ActionList) {
        drawHeroTopPanel(hero)
        val lvl = loadString("lvl")
        var currentY = 4

        //Page title
        ansi.cursor(currentY, 1)
        val diceLimits = loadString("skills_traits")
        ansi.a("│ ")
        ansi.a(diceLimits.toUpperCase()).a(':')
        (3 + diceLimits.length + 1 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')
        currentY++

        //Blank line
        drawBlankLine(currentY++)

        //Main loop
        skills.forEachIndexed { index, skill ->
            val skillName = stringHelper.loadSkillName(skill).toUpperCase()
            //Index
            ansi.a("│   ")
            if (skill.level < skill.maxLevel) {
                ansi.color(LIGHT_YELLOW)
            }
            if (index < 10) {
                ansi.a(String.format(" (%d) ", (index + 1) % 10)).reset()
            } else {
                ansi.a(String.format(" (%c) ", (index - 10 + 'A'.toInt()).toChar())).reset()
            }
            var currentX = 10
            //Name
            ansi.color(Color.WHITE).a(skillName).reset()
            currentX += skillName.length
            //Levels
            if (skill.level > 0) {
                ansi.a(" (").a(lvl).a(' ')
                ansi.color(LIGHT_YELLOW).a(skill.level).reset()
                ansi.a('/').a(skill.maxLevel).a(')')
                currentX += 2 + lvl.length + 5
            }
            (currentX until CONSOLE_WIDTH).forEach { ansi.a(' ') }
            ansi.a('│')
            currentY++
        }

        //Blank lines
        (currentY until CONSOLE_HEIGHT - 3).forEach { drawBlankLine(it) }

        //Separator
        ansi.cursor(CONSOLE_HEIGHT - 3, 1)
        ansi.a('├')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┤')

        //Status and actions
        drawStatusMessage(CONSOLE_HEIGHT - 2, statusMessage)
        drawActionList(CONSOLE_HEIGHT - 1, actions)

        //Bottom border
        ansi.cursor(CONSOLE_HEIGHT, 1)
        ansi.a('└')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┘')

        //Finalize
        render()
    }

    override fun drawSkillInfo(skill: Skill, statusMessage: StatusMessage, actions: ActionList) {
        //Setup
        val name = stringHelper.loadSkillName(skill).toUpperCase()
        val descriptionCurrent = stringHelper.loadSkillDescription(skill, skill.level)
        val descriptionNext = stringHelper.loadSkillDescription(skill, skill.level + 1)
        val descriptionCurrentHeight = if (skill.level > 0) calculateMultilineTextHeight(descriptionCurrent) else 0
        val descriptionNextHeight = calculateMultilineTextHeight(descriptionNext)
        val contentHeight = 3 + descriptionCurrentHeight + 2 + descriptionNextHeight
        var currentY = (CONSOLE_HEIGHT - contentHeight) / 2 - 1

        //Fill blank space
        (1 until currentY - 1).forEach { drawBlankLine(it, false) }

        //Top frame
        ansi.cursor(currentY - 1, 1)
        ansi.a('┌')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┐')

        //Skill name
        ansi.cursor(currentY, 1)
        ansi.a("│ ")
        ansi.color(LIGHT_YELLOW).a(name).reset()
        (3 + name.length until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')
        currentY++
        //Blank line
        drawBlankLine(currentY)
        currentY++
        //Current level
        val currentLevel = loadString("current_level")
        ansi.cursor(currentY, 1)
        ansi.a("│ ").a(currentLevel).a(' ')
        ansi.color(LIGHT_YELLOW).a(skill.level.toString()).reset()
        (3 + currentLevel.length + 2 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')
        currentY++
        if (descriptionCurrentHeight > 0) {
            drawMultilineText(currentY, descriptionCurrent)
        }
        currentY += descriptionCurrentHeight
        //Blank line
        drawBlankLine(currentY)
        currentY++
        //Next level
        val nextLevel = loadString("next_level")
        ansi.cursor(currentY, 1)
        ansi.a("│ ").a(nextLevel).a(' ')
        ansi.color(LIGHT_YELLOW).a((skill.level + 1).toString()).reset()
        (3 + nextLevel.length + 2 until CONSOLE_WIDTH).forEach { ansi.a(' ') }
        ansi.a('│')
        currentY++
        drawMultilineText(currentY, descriptionNext)
        currentY += descriptionNextHeight

        //Bottom frame
        ansi.cursor(currentY, 1)
        ansi.a('└')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┘')
        currentY++

        //Fill blank space
        (currentY until CONSOLE_HEIGHT - 3).forEach { drawBlankLine(it, false) }

        //Separator
        ansi.cursor(CONSOLE_HEIGHT - 3, 1)
        ansi.a('┌')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┐')

        //Status and actions
        drawStatusMessage(CONSOLE_HEIGHT - 2, statusMessage)
        drawActionList(CONSOLE_HEIGHT - 1, actions)

        //Bottom border
        ansi.cursor(CONSOLE_HEIGHT, 1)
        ansi.a('└')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┘')

        //Finalize
        render()
    }

    //------------------------------------------------------------------------------------------------------------------
    override fun drawHeroHandCapacityIncrease(hero: Hero, statusMessage: StatusMessage) {
        val offsetY = (CONSOLE_HEIGHT - 5) / 2
        (1 until offsetY).forEach { drawBlankLine(it, false) }
        ansi.color(heroColors[hero.type])
        drawHorizontalLine(offsetY, '─')
        drawHorizontalLine(offsetY + 4, '─')
        ansi.reset()
        ansi.cursor(offsetY + 1, 1).eraseLine()
        ansi.cursor(offsetY + 3, 1).eraseLine()
        ansi.cursor(offsetY + 2, 1)
        val text = stringHelper.loadStatusMessage(statusMessage)
        val center = (CONSOLE_WIDTH - text.length - hero.name.length - 2) / 2
        ansi.cursor(offsetY + 2, center)
        ansi.eraseLine(Ansi.Erase.BACKWARD)
        ansi.color(heroColors[hero.type]).a(hero.name.toUpperCase()).reset()
        ansi.a(": ")
        with("\\d+".toRegex().find(text)) {
            if (this != null) {
                ansi.a(text.substring(0, this.range.start))
                ansi.color(LIGHT_YELLOW).a(this.value).reset()
                ansi.a(text.substring(this.range.endInclusive + 1))
            } else {
                ansi.a(text)
            }
        }
        ansi.eraseLine(Ansi.Erase.FORWARD)
        (offsetY + 5..CONSOLE_HEIGHT).forEach { drawBlankLine(it, false) }
        render()
    }

    //------------------------------------------------------------------------------------------------------------------
    override fun drawDieTypeSelectionDialog(hero: Hero, statusMessage: StatusMessage, actions: ActionList) {
        //Calculations
        val contentHeight = actions.size
        val contentWidth = 6 + (actions.map { actionDieTitles[it.type]?.length ?: 0 }.max() ?: 0)
        val offsetY = (CONSOLE_HEIGHT - contentHeight) / 2
        val offsetX = (CONSOLE_WIDTH - contentWidth) / 2 - 1

        //Top panel
        drawHeroTopPanel(hero)
        ansi.cursor(3, 1).a('└')
        ansi.cursor(3, CONSOLE_WIDTH).a('┘')

        //Fill blank space
        (4 until offsetY - 2).forEach { drawBlankLine(it, false) }

        //Title
        ansi.cursor(offsetY - 2, offsetX + 2)
        ansi.eraseLine(Ansi.Erase.BACKWARD)
        ansi.a(loadString("choose_die_type"))
        ansi.eraseLine(Ansi.Erase.FORWARD)

        //Top frame
        ansi.cursor(offsetY - 1, offsetX)
        ansi.eraseLine(Ansi.Erase.BACKWARD)
        ansi.a('┌')
        (0 until contentWidth).forEach { ansi.a('─') }
        ansi.a('┐')
        ansi.eraseLine(Ansi.Erase.FORWARD)

        //Dialog contents
        actions.forEachIndexed { index, action ->
            ansi.cursor(offsetY + index, offsetX)
            ansi.eraseLine(Ansi.Erase.BACKWARD)
            ansi.a('│')
            val key = stringHelper.loadActionData(action)[0]
            val label = actionDieTitles[action.type] ?: ""
            ansi.color(LIGHT_YELLOW).a(' ').a(key).a(' ').reset()
            ansi.color(actionDieColors[action.type]).a(label).reset()
            (offsetX + 6 + label.length until offsetX + contentWidth + 2).forEach { ansi.a(' ') }
            ansi.a('│')
            ansi.eraseLine(Ansi.Erase.FORWARD)
        }

        //Bottom frame
        ansi.cursor(offsetY + contentHeight, offsetX)
        ansi.eraseLine(Ansi.Erase.BACKWARD)
        ansi.a('└')
        (0 until contentWidth).forEach { ansi.a('─') }
        ansi.a('┘')
        ansi.eraseLine(Ansi.Erase.FORWARD)

        //Fill blank space
        (offsetY + contentHeight + 1 until CONSOLE_HEIGHT - 3).forEach { drawBlankLine(it, false) }

        //Separator
        ansi.cursor(CONSOLE_HEIGHT - 3, 1)
        ansi.a('┌')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┐')

        //Status and actions
        drawStatusMessage(CONSOLE_HEIGHT - 2, statusMessage)
        drawBlankLine(CONSOLE_HEIGHT - 1)

        //Bottom border
        ansi.cursor(CONSOLE_HEIGHT, 1)
        ansi.a('└')
        (2 until CONSOLE_WIDTH).forEach { ansi.a('─') }
        ansi.a('┘')

        //Finalize
        render()
    }

    override fun drawNewDieReceived(hero: Hero, die: Die) {
        val offsetY = (CONSOLE_HEIGHT - 5) / 2
        (1 until offsetY).forEach { drawBlankLine(it, false) }
        ansi.color(heroColors[hero.type])
        drawHorizontalLine(offsetY, '─')
        drawHorizontalLine(offsetY + 4, '─')
        ansi.reset()
        ansi.cursor(offsetY + 1, 1).eraseLine()
        ansi.cursor(offsetY + 3, 1).eraseLine()
        ansi.cursor(offsetY + 2, 1)
        val receivedNew = loadString("received_new_die")
        val dieName = die.toString()
        val center = (CONSOLE_WIDTH - receivedNew.length - hero.name.length - dieName.length - 4) / 2
        ansi.cursor(offsetY + 2, center)
        ansi.eraseLine(Ansi.Erase.BACKWARD)
        ansi.color(heroColors[hero.type]).a(hero.name.toUpperCase()).reset()
        ansi.a(": ").a(receivedNew).a(' ')
        ansi.color(dieColors[die.type]).a(dieName).reset().a('.')
        ansi.eraseLine(Ansi.Erase.FORWARD)
        (offsetY + 5..CONSOLE_HEIGHT).forEach { drawBlankLine(it, false) }
        render()
    }

}