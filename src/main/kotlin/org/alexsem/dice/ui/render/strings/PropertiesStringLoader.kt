package org.alexsem.dice.ui.render.strings

import java.util.Locale
import java.util.ResourceBundle

/**
 * Implementation of [StringLoader] which uses resource files for localization
 */
class PropertiesStringLoader(locale: Locale) : StringLoader {
    private val properties = ResourceBundle.getBundle("text.strings", locale, Utf8ResourceBundleControl())

    override fun loadString(key: String) = properties.getString(key) ?: ""
}
