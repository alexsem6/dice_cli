package org.alexsem.dice.ui.render.strings

import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.util.*

class Utf8ResourceBundleControl : ResourceBundle.Control() {

    @Throws(IllegalAccessException::class, InstantiationException::class, IOException::class)
    override fun newBundle(baseName: String, locale: Locale, format: String, loader: ClassLoader, reload: Boolean): ResourceBundle? {
        val bundleName = toBundleName(baseName, locale)
        return when (format) {
            "java.class" -> super.newBundle(baseName, locale, format, loader, reload)
            "java.properties" ->
                with((if ("://" in bundleName) null else toResourceName(bundleName, "properties")) ?: return null) {
                    when {
                        reload -> reload(this, loader)
                        else -> loader.getResourceAsStream(this)
                    }?.let { stream -> InputStreamReader(stream, "UTF-8").use { r -> PropertyResourceBundle(r) } }
                }
            else -> throw IllegalArgumentException("Unknown format: $format")
        }
    }

    @Throws(IOException::class)
    private fun reload(resourceName: String, classLoader: ClassLoader): InputStream {
        classLoader.getResource(resourceName)?.let { url ->
            url.openConnection().let { connection ->
                connection.useCaches = false
                return connection.getInputStream()
            }
        }
        throw IOException("Unable to load data!")
    }

}