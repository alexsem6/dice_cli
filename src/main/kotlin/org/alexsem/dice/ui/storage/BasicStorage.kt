package org.alexsem.dice.ui.storage

import org.alexsem.dice.generator.template.adventure.AdventureTemplate
import org.alexsem.dice.model.Hero
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*
import java.util.stream.Collectors

private const val PARTY_EXTENSION = ".party"
private const val GAME_EXTENSION = ".game"

/**
 * Basic [Storage] implementation. Stores save files in directory, specified in constructor
 */
class BasicStorage(savesDir: String) : Storage {

    private val savesDir = Paths.get(savesDir)

    /**
     * Get path to save file for specific party
     * @param id Party identifier
     * @return path to file
     */
    private fun getPathToParty(id: String) = savesDir.resolve(id + PARTY_EXTENSION)

    /**
     * Get path to save file for specific party
     * @param id Party identifier
     * @return path to file
     */
    private fun getPathToGame(id: String) = savesDir.resolve(id + GAME_EXTENSION)

    @Throws(IOException::class)
    override fun createParty(name: String, heroes: List<Hero>, adventure: AdventureTemplate): PartyInfoFull {
        if (Files.notExists(savesDir)) {
            Files.createDirectories(savesDir)
        }
        val id = generatePartyIdentifier(name)
        val dataShort = PartyInfoShort(id).apply {
            this.timestamp = Date()
            this.partyName = name
            this.heroNames = heroes.map { it.name }
            this.heroTypes = heroes.map { it.type }
            this.adventure = adventure
        }
        val dataFull = PartyInfoFull().apply {
            this.id = id
            this.partyName = name
            this.heroes = heroes
            this.adventure = adventure
            this.progress = 0
        }
        Files.newOutputStream(getPathToParty(id)).use {
            writePartyInfoShort(it, dataShort)
            writePartyInfoFull(it, dataFull)
        }
        return dataFull
    }

    @Throws(IOException::class)
    override fun updateParty(info: PartyInfoFull) {
        val dataShort = PartyInfoShort(info.id).apply {
            this.timestamp = Date()
            this.partyName = info.partyName
            this.heroNames = info.heroes.map { it.name }
            this.heroTypes = info.heroes.map { it.type }
            this.adventure = info.adventure
        }
        Files.newOutputStream(getPathToParty(info.id)).use {
            writePartyInfoShort(it, dataShort)
            writePartyInfoFull(it, info)
        }
    }

    @Throws(IOException::class)
    override fun deleteParty(id: String) {
        Files.deleteIfExists(getPathToGame(id))
        Files.deleteIfExists(getPathToParty(id))
    }

    @Throws(IOException::class)
    override fun loadParty(id: String) =
            Files.newInputStream(getPathToParty(id)).use {
                readPartyInfoShort(it)
                readPartyInfoFull(it)
            }

    @Throws(IOException::class)
    override fun loadPartyList(): List<PartyInfoShort> {
        if (Files.notExists(savesDir)) {
            Files.createDirectories(savesDir)
        }
        return Files.list(savesDir)
                .filter { it.toString().endsWith(PARTY_EXTENSION) }
                .map { path ->
                    Files.newInputStream(path).use {
                        readPartyInfoShort(it)
                    }
                }
                .sorted { o1, o2 -> o2.timestamp.compareTo(o1.timestamp) }
                .collect(Collectors.toList())
    }

    override fun isPartiesAvailable() = try {
        Files.list(savesDir).anyMatch { it.toString().endsWith(PARTY_EXTENSION) }
    } catch (ex: Exception) {
        false
    }

    //------------------------------------------------------------------------------------------------------------------

    @Throws(IOException::class)
    override fun deleteGame(id: String) {
        Files.delete(getPathToGame(id))
    }

    override fun loadGame(id: String) =
            Files.newInputStream(getPathToGame(id)).use {
                readGameInfo(it)
            }

    override fun saveGame(info: GameInfo) {
        Files.newOutputStream(getPathToGame(info.partyId)).use {
            writeGameInfo(it, info)
        }
    }

    override fun isGameSaveAvailable(id: String) = try {
        Files.exists(getPathToGame(id))
    } catch (ex: Exception) {
        false
    }
}